import { Component } from '@angular/core';
import { Router, NavigationEnd, NavigationStart, Event } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'student-healthcare';
  currentUrl = "/";
  currentUrl2 = "dashboard";
  currentUrl3 = "doctor_assigned_school";
  currentUrl4 = "school_admin";
  currentUrl5 = "student_health_care";

  userAuth: any;
  private sub: any;


  constructor(private router: Router) {
    this.sub = router.events.subscribe((event: Event) => {
      // console.log(event);
      if (event instanceof NavigationStart) {
        this.currentUrl = event.url;
        this.currentUrl2 = event.url;
        this.currentUrl3 = event.url;
        this.currentUrl4 = event.url;
        this.currentUrl5 = event.url
        console.log("current URl", this.currentUrl);
      }
    });
    // this.userAuth = JSON.parse(localStorage.getItem("user"))
    // console.log('userAuth from amin', this.userAuth)
  }
}
