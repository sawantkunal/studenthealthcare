import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoverpasswordEmailComponent } from './recoverpassword-email.component';

describe('RecoverpasswordEmailComponent', () => {
  let component: RecoverpasswordEmailComponent;
  let fixture: ComponentFixture<RecoverpasswordEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoverpasswordEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoverpasswordEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
