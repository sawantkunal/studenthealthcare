import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './components/_layout/admin-layout/admin-layout.component';
import { SchoolLayoutComponent } from './components/_layout/school-layout/school-layout.component';
import { StudentLayoutComponent } from './components/_layout/student-layout/student-layout.component';
import { DoctorsLayoutComponent } from './components/_layout/doctors-layout/doctors-layout.component';

import { DashboardComponent } from './modules/super-admin/dashboard/dashboard.component';
// import { HeaderComponent } from './components/_layout/admin-header/header.component';
import { LoginComponent } from './components/login/login.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { RecoverpasswordEmailComponent } from './components/recoverpassword-email/recoverpassword-email.component';
import { DoctorDashboardComponent } from './modules/doctor/doctor-dashboard/doctor-dashboard.component';
import { SchoolDashboardComponent } from './modules/school-admin/school-dashboard/school-dashboard.component';
// import { AuthGuard } from './guards/auth.guard';

import { AuthAdminGuard } from './guards/auth-admin.guard';
import { AuthDoctorGuard } from './guards/auth-doctor.guard';
import { AuthSchoolGuard } from './guards/auth-school.guard';

import { DoctorAssignedSchoolsComponent } from './modules/doctor/doctor-assigned-schools/doctor-assigned-schools.component';
import { TodaysBookingsComponent } from "./modules/doctor/todays-bookings/todays-bookings.component";
import { SchoolAdminComponent } from './modules/school-admin/school-admin/school-admin.component';

import { AddSchoolComponent } from './modules/super-admin/add-school/add-school.component';
import {UpdateSchoolComponent} from './modules/super-admin/update-school/update-school.component';
import { SchoolListComponent } from './modules/super-admin/school-list/school-list.component';
import { AddStudentComponent } from './modules/super-admin/add-student/add-student.component';
import { StudentListComponent } from './modules/super-admin/student-list/student-list.component';
import { AddDoctorComponent } from './modules/super-admin/add-doctor/add-doctor.component';
import { DoctorListComponent } from './modules/super-admin/doctor-list/doctor-list.component';
import { StudentProfileComponent } from './modules/super-admin/student-profile/student-profile.component';
import { DoctorProfileComponent } from './modules/super-admin/doctor-profile/doctor-profile.component';
import { ReportHistoryComponent } from './components/report-history/report-history.component';
import { SchoolDetailsComponent } from './modules/super-admin/school-details/school-details.component'
import { SchoolReportComponent } from './components/school-report/school-report.component';
import { AddClassesComponent } from './modules/super-admin/add-classes/add-classes.component';
import { ClassListComponent } from './modules/super-admin/class-list/class-list.component';
import { ScheduleCheckupComponent} from './modules/super-admin/schedule-checkup/schedule-checkup.component';
import { ScheduleListComponent} from './modules/super-admin/schedule-list/schedule-list.component';
import { CheckupDoneListComponent } from './modules/super-admin/checkup-done-list/checkup-done-list.component';

import { StudentReportComponent } from './modules/doctor/student-report/student-report.component';
import { StudentCheckupComponent } from './modules/doctor/student-checkup/student-checkup.component';
import { UploadDocComponent } from './components/upload-doc/upload-doc.component';
import { StudentListComponents } from './modules/school-admin/student-list/student-list.component';
import { StudentProfileComponents } from './modules/school-admin/student-profile/student-profile.component'

import { SchoolstudentListComponent } from './modules/doctor/schoolstudent-list/schoolstudent-list.component';
import { PastassignedschoolListComponent } from './modules/doctor/pastassignedschool-list/pastassignedschool-list.component'
import { PastschoolstudentListComponent } from './modules/doctor/pastschoolstudent-list/pastschoolstudent-list.component';
import { PaststudentcheckupComponent } from './modules/doctor/paststudentcheckup/paststudentcheckup.component';
import { StudentDonecheckupListComponent } from './modules/super-admin/student-donecheckup-list/student-donecheckup-list.component';
import { StudentCheckupDetailComponent } from './modules/super-admin/student-checkup-detail/student-checkup-detail.component';
import { AddDivisionComponent } from "./modules/super-admin/add-division/add-division.component";

const appRoutes: Routes = [

  {
    path: '',
    canActivate: [AuthAdminGuard],
    component: AdminLayoutComponent,
    children: [
      { path: 'admin-dashboard', canActivate: [AuthAdminGuard], component: DashboardComponent },
      { path: 'home', canActivate: [AuthAdminGuard], component: DashboardComponent },

      { path: 'add_school', canActivate: [AuthAdminGuard], component: AddSchoolComponent },
      { path: 'add_classes', canActivate: [AuthAdminGuard], component: AddClassesComponent },
      { path: 'add_classes/:id', canActivate: [AuthAdminGuard], component: AddClassesComponent },
      { path: 'add_division', canActivate: [AuthAdminGuard], component: AddDivisionComponent },
      { path: 'add_division/:id', canActivate: [AuthAdminGuard], component: AddDivisionComponent },
      { path: 'update_class/:id', canActivate: [AuthAdminGuard], component: AddClassesComponent },
      { path: 'classes_list', canActivate: [AuthAdminGuard], component: ClassListComponent },
      { path: 'update_school/:id', canActivate: [AuthAdminGuard], component: UpdateSchoolComponent },
      
      
      { path: 'schedule_checkup', canActivate: [AuthAdminGuard], component: ScheduleCheckupComponent },
      { path: 'update_schedule/:id', canActivate: [AuthAdminGuard], component: ScheduleCheckupComponent },
      { path: 'schedule_list', canActivate: [ AuthAdminGuard], component: ScheduleListComponent },
      { path: 'add_school/:id', canActivate: [ AuthAdminGuard], component: AddSchoolComponent },
      { path: 'school_list', canActivate: [ AuthAdminGuard], component: SchoolListComponent },
      { path: 'add_student', canActivate: [ AuthAdminGuard], component: AddStudentComponent },
      { path: 'add_student/:id', canActivate: [ AuthAdminGuard], component: AddStudentComponent },
      { path: 'student_list', canActivate: [ AuthAdminGuard], component: StudentListComponent },
      { path: 'add_doctor', canActivate: [ AuthAdminGuard], component: AddDoctorComponent },
      { path: 'add_doctor/:id', canActivate: [ AuthAdminGuard], component: AddDoctorComponent },
      { path: 'doctor_list', canActivate: [ AuthAdminGuard], component: DoctorListComponent },
      { path: 'student_profile/:id', canActivate: [ AuthAdminGuard], component: StudentProfileComponent },
      { path: 'doctor_profile/:id', canActivate: [ AuthAdminGuard], component: DoctorProfileComponent },
      { path: 'checkup_history/:id', canActivate: [ AuthAdminGuard], component: ReportHistoryComponent },
      { path: 'school_details/:id', canActivate: [ AuthAdminGuard], component: SchoolDetailsComponent },
      { path: 'upload_doc', canActivate: [ AuthAdminGuard], component: UploadDocComponent },
      { path : 'done_checkups', canActivate: [ AuthAdminGuard], component: CheckupDoneListComponent },
      { path : 'studentcheckuplist/:id', canActivate: [ AuthAdminGuard], component: StudentDonecheckupListComponent },
      { path : 'student_checkup_detail/:id', canActivate: [ AuthAdminGuard], component:StudentCheckupDetailComponent},
    ]
  },

  // App routes goes here here
  {
    path: '',
    canActivate: [AuthSchoolGuard],
    component: SchoolLayoutComponent,
    children: [
      // , pathMatch: 'full'
      { path: 'school_dashboard', canActivate: [AuthSchoolGuard], component: SchoolDashboardComponent },
      { path: 'school_admin', canActivate: [AuthSchoolGuard], component: SchoolAdminComponent },
      { path: 'school_students_list', canActivate: [AuthSchoolGuard], component: StudentListComponents },
      { path: 'school_students_details/:id', canActivate: [AuthSchoolGuard], component: StudentProfileComponents },
      { path: 'school_report', canActivate: [AuthSchoolGuard], component: SchoolReportComponent },
    ]
  },

  // {
  //   path: '',
  //   canActivate: [AuthSchoolGuard],
  //   component: StudentLayoutComponent,
  //   children: [

  //   ]
  // },

  // App routes goes here here
  {
    path: '',
    component: DoctorsLayoutComponent,
    canActivate:[AuthDoctorGuard],
    children: [
      { path: 'doctor-dashboard', canActivate: [AuthDoctorGuard], component: DoctorDashboardComponent },
      { path: 'doctor_assigned_todays_school', canActivate: [AuthDoctorGuard], component: TodaysBookingsComponent },
      { path: 'doctor_assigned_school', canActivate: [AuthDoctorGuard], component: DoctorAssignedSchoolsComponent },
      { path: 'pastdoctor_assigned_school', canActivate: [AuthDoctorGuard], component: PastassignedschoolListComponent },
      { path: 'student_list/:id', canActivate: [AuthDoctorGuard], component: SchoolstudentListComponent },
      { path: 'paststudent_list/:id', canActivate: [AuthDoctorGuard], component: PastschoolstudentListComponent },
      { path: 'student_checkup', canActivate: [AuthDoctorGuard], component: StudentCheckupComponent },
      { path: 'student_checkup/:id', canActivate: [AuthDoctorGuard], component: StudentCheckupComponent },
      { path: 'paststudent_checkup/:id', canActivate: [AuthDoctorGuard], component: PaststudentcheckupComponent},
      { path: 'student_checkup/:id/:userId/:sname', canActivate: [AuthDoctorGuard], component: StudentCheckupComponent },
      { path: 'student_report', canActivate: [AuthDoctorGuard], component: StudentReportComponent },
    ]
  },

  // { path: '**', redirectTo: '' },
  { path: 'login', component: LoginComponent },
  { path: 'forgot_password/:id', component: ForgotPasswordComponent },
  { path: 'forgot_password_email', component: RecoverpasswordEmailComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
