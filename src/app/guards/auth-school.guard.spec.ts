import { TestBed, async, inject } from '@angular/core/testing';

import { AuthSchoolGuard } from './auth-school.guard';

describe('AuthSchoolGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthSchoolGuard]
    });
  });

  it('should ...', inject([AuthSchoolGuard], (guard: AuthSchoolGuard) => {
    expect(guard).toBeTruthy();
  }));
});
