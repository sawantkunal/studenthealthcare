import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthDoctorGuard implements CanActivate {

  constructor(private routes:Router){} 

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      var user = JSON.parse(localStorage.getItem("user"))
      if(user && user !== null && user.user_type_id == 2){
        return true;
      }else{
        this.routes.navigate(['login']);
        return false;
      }
  }
}
