import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private routes:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      var user = JSON.parse(localStorage.getItem("user"))
      if(user && user !== null && user.user_type_id == 1){
        return true;
      }else{
        this.routes.navigate(['login']);
        return false;
      }
  }
}
