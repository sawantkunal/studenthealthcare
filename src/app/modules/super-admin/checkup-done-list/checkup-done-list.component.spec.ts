import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckupDoneListComponent } from './checkup-done-list.component';

describe('CheckupDoneListComponent', () => {
  let component: CheckupDoneListComponent;
  let fixture: ComponentFixture<CheckupDoneListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckupDoneListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckupDoneListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
