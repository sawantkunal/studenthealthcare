import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/user.service';

declare let $;

@Component({
  selector: 'app-checkup-done-list',
  templateUrl: './checkup-done-list.component.html',
  styleUrls: ['./checkup-done-list.component.css']
})
export class CheckupDoneListComponent implements OnInit {

  checkuplist:any;
  table: any;
  data;
  filterData;

  constructor(private router: Router, private userService: UserService,
    private toastr: ToastrService, private _fb: FormBuilder,
    private active: ActivatedRoute,) { }

  ngOnInit() {
    this.getDonecheckuplist();
  }
  getDonecheckuplist(){
    var that = this;
    this.userService.getDonecheckuplist()
        .subscribe(res => {
          if (res.status == 200) {
            for (let i = 0; i < res.body['data'].length; i++) {
              let sr = {
                sr_no: i + 1
              }
              Object.assign(res.body['data'][i], sr);
            }
            this.data = res.body['data'];
            this.filterData = res.body['data'];
            console.log('response school list', res.body['data']);
            setTimeout(function() {
              this.table = $("#checkupTable").DataTable({
                dom: "Bfrtip",
                aoColumnDefs: [
                  {
                    bSortable: false,
                    aTargets: [-1]
                  }
                ],
                retrieve: true,
                destroy: true
              });
              that.table = this.table;
            }, 200);
          }
        })
  }
}
