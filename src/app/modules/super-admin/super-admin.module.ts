import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddSchoolComponent } from './add-school/add-school.component';
import { SchoolListComponent } from './school-list/school-list.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { StudentListComponent } from './student-list/student-list.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { SchoolDetailsComponent } from './school-details/school-details.component'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {DataTableModule} from "angular-6-datatable";

// import { routing } from './super-admin.module.route';
import { UpdateSchoolComponent } from './update-school/update-school.component';
import { ClassListComponent } from './class-list/class-list.component';
import { ScheduleCheckupComponent } from './schedule-checkup/schedule-checkup.component';
import { ScheduleListComponent } from './schedule-list/schedule-list.component';
import { CheckupDoneListComponent } from './checkup-done-list/checkup-done-list.component';
import { StudentDonecheckupListComponent } from './student-donecheckup-list/student-donecheckup-list.component';
import { StudentCheckupDetailComponent } from './student-checkup-detail/student-checkup-detail.component';
import { AddDivisionComponent } from './add-division/add-division.component';

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    // routing,
    DataTableModule,
    NgMultiSelectDropDownModule.forRoot(),

  ],
  declarations: [
    AddSchoolComponent,
    SchoolListComponent,
    AddStudentComponent,
    StudentListComponent,
    AddDoctorComponent,
    DoctorListComponent,
    StudentProfileComponent,
    DoctorProfileComponent,
    SchoolDetailsComponent,
    UpdateSchoolComponent,
    ClassListComponent,
    ScheduleCheckupComponent,
    ScheduleListComponent,
    CheckupDoneListComponent,
    StudentDonecheckupListComponent,
    StudentCheckupDetailComponent,
    AddDivisionComponent,
  ],
  exports: [
    AddSchoolComponent,
    SchoolListComponent,
    AddStudentComponent,
    StudentListComponent,
    AddDoctorComponent,
    DoctorListComponent,
    StudentProfileComponent,
    DoctorProfileComponent,
    ReactiveFormsModule,
    FormsModule,
    SchoolDetailsComponent,
    DataTableModule,
    AddDivisionComponent,
    
  ]
})
export class SuperAdminModule { }
