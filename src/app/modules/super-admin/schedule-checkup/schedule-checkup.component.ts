import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/user.service';
import { t } from '@angular/core/src/render3';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-schedule-checkup',
  templateUrl: './schedule-checkup.component.html',
  styleUrls: ['./schedule-checkup.component.css']
})
export class ScheduleCheckupComponent implements OnInit {

  addScheduleForm: any;
  userId: any;
  formData: any;
  submitted = false;
  doctorList: any;
  school_list: any;
  classList: any;
  class_id: any;
  class_name:any;
  divisionList = [];
  disableButton:boolean;
  checkupId;

  constructor(private router: Router, private userService: UserService,
    private toastr: ToastrService, private _fb: FormBuilder,
    private active: ActivatedRoute, ) { }

  ngOnInit() {


    this.getSchoolList();
    this.getDoctorList();
    
    this.scheduleFormData();

   
    this.active.params.subscribe(params => {
      this.checkupId = params['id'];
    });

    if(this.checkupId){
      console.log('this.checkupId',this.checkupId)
      this.userService.getScheduledCheckup(this.checkupId)
        .subscribe(res =>{
          if(res.status == 200){
            
            this.addScheduleForm.patchValue(res.body['data']);
            this.getclassBySchool(res.body['data']['school_id']);
            if(res.body['data']['class_id'] != null){
              this.getDivisionByClass(res.body['data']['class_id']);
            }
            console.log('this.school_id',res.body['data']['school_id']);
          }
        })
    }

    // this.getUpdateSchedule();


  }
  getclassBySchool(schoolId){
    this.divisionList = [];
    this.classList = [];
    this.userService.getClass(schoolId)
              .subscribe(res => {
                if (res.status == 200) {
                  this.classList = res.body['data'];
                }else{
                  this.classList = [];
                }
              });
  }
  getDivisionByClass(classId){
    this.divisionList = [];
    this.userService.getDivision(classId)
      .subscribe(res => {
        if(res.status == 200){
          this.divisionList = res.body['data'];
        }else if(res.status == 202){
          this.divisionList = [];
        }
      });
  }
  getUpdateSchedule() {
    if (this.userId) {
      this.userService.getScheduleById(this.userId)
        .subscribe(res => {
          console.log('response',res.body['data']);
          this.addScheduleForm.patchValue(res.body['data']);
          //this.class_id = res.body['data']['class_id'];
          this.classList = res.body['class_list'];
          this.divisionList = res.body['division_list']
          console.log('this.divisionList',this.divisionList);
          // res.body['class_list'].forEach(element => {
          //   if(element.class_id == this.class_id){
          //     this.class_name = element.class_name
          //   }
          // });
          this.addScheduleForm.patchValue(res.body['data'][0]);
        })
    }
  }

  getDivision(event) {
    this.divisionList = [];
    console.log('getDivision',event.target.value);
    if(event.target.value){
      this.userService.getDivision(event.target.value)
      .subscribe(res => {
        if(res.status == 200){
          this.divisionList = res.body['data'];
          // console.log('res',res)
        }else if(res.status == 202){
          this.divisionList = [];
          this.toastr.error(res.body['message'],'',{timeOut:600});;
        }
        
        console.log('getDivision', res.body['data'])
      })
    }
  }

  addScheduleData() {
    this.submitted = true;
    
    if (this.addScheduleForm.valid) {
      this.disableButton =true;
      this.submitted = false;
      this.userService.addSchoolCheckup(this.addScheduleForm.value)
        .subscribe(res => {
          if (res.status == 200) {
            console.log('res',res);
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.router.navigate(['schedule_list']);
          }else{
            this.toastr.error(res.body['message'],'',{timeOut:600});;
            this.disableButton =false;
          }
          // console.log('response is here', res.body)
        })
    }
  }
  getToday(): string {
    return new Date().toISOString().split('T')[0]
 }
  updateScheduleData() {
    this.submitted = true;
    
    this.addScheduleForm.value.scheduled_checkup_id = this.userId;

    if (this.addScheduleForm.valid) {
      // console.log('lskajdfkljlasdf', this.addScheduleForm.value)

      this.submitted = false;
      this.disableButton = true;
      this.userService.updateScheduleCheckup(this.addScheduleForm.value)
        .subscribe(res => {
          if (res.status == 200 ) {
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.router.navigate(['schedule_list']);
          }else{
            this.toastr.error(res.body['message'],'',{timeOut:600});;
          }
          // console.log('response is here', res.body)
        })

    }
  }

  getDoctorList() {
    this.userService.getDoctor()
      .subscribe(res => {
        if (res.status == 200) {
          this.doctorList = res.body['data'];
        }
         console.log('getDoctorList', res);
      })
  }

  getSchoolList() {
    this.userService.getSchool()
      .subscribe(res => {
        if (res.status == 200) {
          this.school_list = res.body['data'];
          this.classList = [];
          this.divisionList = [];
        }
        console.log('this.school_list', res.body['data']);
      })
  }

  getClass(event) {
    console.log('getclass',event.target.value)
    this.classList = [];
    this.divisionList = [];
    if(event.target.value){
      this.userService.getClass(event.target.value)
      .subscribe(res => {
        if (res.status == 200) {
          this.classList = res.body['data'];
          this.divisionList = [];
        }else{
          this.classList = [];
          this.divisionList = [];
          this.addScheduleForm.value.division_id = null;
        }
      })
    }
  }

  getClasses(event) {
    if(event){
      this.userService.getClass(event)
        .subscribe(res => {
          this.classList = res.body['data'];
          this.divisionList = [];
          // console.log('getClass', res.body['data'])
        })
    }
  }

  scheduleFormData() {
    this.addScheduleForm = this._fb.group({
      school_id: ['', Validators.compose([Validators.required])],
      dr_id: ['', Validators.compose([Validators.required])],
      class_id: [''],
      division_id: [''],
      scheduled_at: ['', Validators.compose([Validators.required])],
    })
  }
  get f() { return this.addScheduleForm.controls }
  get g() { return this.addScheduleForm.value }

}
