import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleCheckupComponent } from './schedule-checkup.component';

describe('ScheduleCheckupComponent', () => {
  let component: ScheduleCheckupComponent;
  let fixture: ComponentFixture<ScheduleCheckupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleCheckupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleCheckupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
