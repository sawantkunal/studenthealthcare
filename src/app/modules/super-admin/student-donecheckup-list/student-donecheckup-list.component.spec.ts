import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentDonecheckupListComponent } from './student-donecheckup-list.component';

describe('StudentDonecheckupListComponent', () => {
  let component: StudentDonecheckupListComponent;
  let fixture: ComponentFixture<StudentDonecheckupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentDonecheckupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentDonecheckupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
