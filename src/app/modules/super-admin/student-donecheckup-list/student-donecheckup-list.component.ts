import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/user.service';

declare let $;

@Component({
  selector: 'app-student-donecheckup-list',
  templateUrl: './student-donecheckup-list.component.html',
  styleUrls: ['./student-donecheckup-list.component.css']
})
export class StudentDonecheckupListComponent implements OnInit {

  constructor(private router: Router, private userService: UserService,
    private toastr: ToastrService, private _fb: FormBuilder,
    private active: ActivatedRoute,) { }
  schId :any;
  table: any;
  filterData = [];
  data = [];
  studentList :any;
  userToken :any;

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    console.log('userForm this.userToken', this.userToken);
    this.active.params.subscribe(params => {
      this.schId = params['id'];
    });

    if(this.schId){
      var that = this;
      this.userService.getcheckupStudentListByID(this.schId)
        .subscribe(res =>{
// console.log("getcheckupStudentList res",res);
          if (res.status == 200 && res.body['data'] != null) {
            for (let i = 0; i < res.body['data'].length; i++) {
              let sr = {
                sr_no: i + 1
              }
              Object.assign(res.body['data'][i], sr);
            }
            res.body['data'].forEach(element => {
              this.filterData.push(element);
              this.data.push(element);

            });
            console.log('response is getting1111111111', this.data);
            setTimeout(function() {
              this.table = $("#studentTable").DataTable({
                dom: "Bfrtip",
                aoColumnDefs: [
                  {
                    bSortable: false,
                    aTargets: [-1]
                  }
                ],
                retrieve: true,
                destroy: true
              });
              that.table = this.table;
            }, 200);
          }
          else if(res.status == 202){
            console.log('response is getting', res);
          }
        })
    }
  }

}
