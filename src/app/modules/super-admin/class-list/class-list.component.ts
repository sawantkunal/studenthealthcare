import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from "@angular/router";

declare let $;

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.css']
})
export class ClassListComponent implements OnInit {

  data;
  filterData;
  table: any;

  constructor(private router: Router, private userService: UserService, ) { }

  ngOnInit() {
    this.getClassList();
  }

  getClassList() {
    var that = this;
    this.userService.getClassList()
      .subscribe(res => {
        console.log('response school list', res);

        if (res.status == 200) {
          for (let i = 0; i < res.body['data'].length; i++) {
            let sr = {
              sr_no: i + 1
            }
            Object.assign(res.body['data'][i], sr);
          }
          this.data = res.body['data'];
          this.filterData = res.body['data'];
          console.log('response school list', res.body['data']);
          setTimeout(function() {
            this.table = $("#classTable").DataTable({
              dom: "Bfrtip",
              aoColumnDefs: [
                {
                  bSortable: false,
                  aTargets: [-1]
                }
              ],
              retrieve: true,
              destroy: true
            });
            that.table = this.table;
          }, 200);
        }

      })
  }

  search(term: string) {
    if (!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
        x.Student_first.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }

}
