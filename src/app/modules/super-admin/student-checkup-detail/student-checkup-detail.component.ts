import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-student-checkup-detail',
  templateUrl: './student-checkup-detail.component.html',
  styleUrls: ['./student-checkup-detail.component.css']
})
export class StudentCheckupDetailComponent implements OnInit {
  addStudentCheckupForm: FormGroup;
  formData: any;
  submitted = false;
  userToken: any;
  sname: any;
  User_id: any;
  userId: any;
  checkupId: any;
  studCheckup: any;
  cities = [];
  Student_id: any;
  studentData: any;
  bmi: any;
  bmr: any;
  blurWeightEvent = false;
  blurHeightEvent = false;
  age:any;
  tbw:any;
  sch_checkupId:any;

  constructor(private toastr: ToastrService, private active: ActivatedRoute, 
    private router: Router, private userService: UserService, private _fb: FormBuilder) { }

  ngOnInit() {
    this.studentCheckUpFormData();
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
      this.checkupId = params['id'];
      if (this.checkupId) {
        this.userService.getCheckupDetail(this.checkupId)
          .subscribe(res => {
            if (res.status == 200) {
              console.log('response is getting', res.body['data'][0]);
              this.addStudentCheckupForm.patchValue(res.body['data'][0]);
              this.age = res.body['age'];
              this.studentData = res.body['data'][0];
              this.sch_checkupId = res.body['data']['0']['scheduled_checkup_id'];
            }
            else if (res.status == 202) {
              console.log('response is getting', res);
            }
          })
      }
    })
  }
  submitStudentCheckup() {
    // this.submitted = true;
    console.log('this.', this.g)

    if (this.g.height != 0 && this.g.weight != 0 && this.g.heart_rate != 0 && this.g.bp != 0 && 
        this.g.dental_report != 0 && this.g.visceral_fat != 0 && this.g.spherical_left != 0 && this.g.spherical_right != 0 &&
        this.g.cylindrical_left != 0 && this.g.cylindrical_right != 0 && this.g.axis_left != 0 && this.g.axis_right != 0 &&
        this.g.ap_left != 0 && this.g.ap_right != 0 && this.g.suggested_treatment && this.g.consultation_notes) {

      this.submitted = false;
      this.blurHeightEvent = false;      
      this.addStudentCheckupForm.value.Token = this.userToken.jwt_token;
      this.addStudentCheckupForm.value.usertype = this.userToken.user_type_id;
      this.addStudentCheckupForm.value.checkupId = this.checkupId;

      if(this.bmr !=null && this.bmi !=null && this.tbw!=null){
        this.addStudentCheckupForm.value.basal_metabolism = this.bmr
        this.addStudentCheckupForm.value.bmi = this.bmi;
        this.addStudentCheckupForm.value.water = this.tbw;
      }
      
      this.userService.postStudentCheckup(this.addStudentCheckupForm.value)
        .subscribe(res => {
          this.toastr.success(res['message']);
         this.router.navigate(['student_list/' + res['schId']]);

           console.log('response is here', res)
        })
    } else {
      this.blurHeightEvent = true;
      this.submitted = true;
      this.toastr.error('Value should not be 0');
      // console.log('else', this.addStudentCheckupForm.valid)

    }
  }
  onItemSelect(e) {
    this.Student_id = e.target.value
  }
  getStudentBySchool() {
    this.userService.getStudentBySchool(this.userToken.jwt_token, this.userToken.user_type_id, this.sname)
      .subscribe(res => {
        this.studCheckup = res['data'];

        console.log('response getting', this.studCheckup);

      })
  }

  getBodyDetail() {
    var height = this.addStudentCheckupForm.value.height;
    var weight = this.addStudentCheckupForm.value.weight;

    if (height == 0 ) {
      this.blurHeightEvent = true;
    }else if(weight == 0){
      this.blurWeightEvent = true;
    }else if(height ==0 && weight ==0){
      this.blurHeightEvent = true;
      this.blurWeightEvent = true;
    }else if(height !=0 && weight !=0){
      this.bmr = 66.47 + (13.75 * weight) + (5.003 * height) - (6.755 * this.age);
      this.bmi = (weight / height / height) * 10000;  

      //Total Body Water
      if(this.studentData.gender=='male'){  
          this.tbw = 2.447 - 0.09156 * this.age + 0.1074 * height + 0.3362 * weight;
      }else if(this.studentData.gender=='female'){
        this.tbw = -2.097 + 0.1069 * height + 0.2466 * weight;
      }

      this.blurHeightEvent = false;
      this.blurWeightEvent = false;
    }

  }

  studentCheckUpFormData() {
    this.addStudentCheckupForm = this._fb.group({
      Student_id: [''],
      spherical_left: ['', Validators.compose([Validators.required])],
      spherical_right: ['', Validators.compose([Validators.required])],
      cylindrical_left: ['', Validators.compose([Validators.required])],
      cylindrical_right: ['', Validators.compose([Validators.required])],
      axis_left: ['', Validators.compose([Validators.required])],
      axis_right: ['', Validators.compose([Validators.required])],
      ap_left: ['', Validators.compose([Validators.required])],
      ap_right: ['', Validators.compose([Validators.required])],
      height: ['', Validators.compose([Validators.required])],
      weight: ['', Validators.compose([Validators.required])],
      // Validators.pattern('^[0-9]*$'
      bmi: [''],
      bp: ['', Validators.compose([Validators.required])],
      heart_rate: ['', Validators.compose([Validators.required])],
      muscle_mass: [''],
      body_fat: [''],
      water: [''],
      basal_metabolism: [''],
      bone_mass: [''],
      dental_report: ['', Validators.compose([Validators.required])],
      consultation_notes: ['', Validators.compose([Validators.required])],
      visceral_fat: ['', Validators.compose([Validators.required])],
      suggested_treatment: ['', Validators.compose([Validators.required])],
    })
  }
  get f() { return this.addStudentCheckupForm.controls; }
  get g() { return this.addStudentCheckupForm.value }
}
