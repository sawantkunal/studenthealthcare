import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentCheckupDetailComponent } from './student-checkup-detail.component';

describe('StudentCheckupDetailComponent', () => {
  let component: StudentCheckupDetailComponent;
  let fixture: ComponentFixture<StudentCheckupDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentCheckupDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentCheckupDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
