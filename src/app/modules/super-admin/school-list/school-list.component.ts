import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { UserService } from '../../../services/user.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

declare let $;

@Component({
  selector: 'app-school-list',
  templateUrl: './school-list.component.html',
  styleUrls: ['./school-list.component.css']
})
export class SchoolListComponent implements OnInit {
  schoolList: any;
  userToken: any;
  // filterData:any;

  dataUrl = 'https://jsonplaceholder.typicode.com/users';
  data;
  filterData;
  table: any;

  constructor(private userService: UserService, private http: HttpClient,private toastr: ToastrService) { }

  ngOnInit() {

    this.userToken = JSON.parse(localStorage.getItem("user"));
    this.getSchoolList();
  }

  getSchoolList() {
    console.log('response')
    var that = this;
    this.userService.getSchoolList()
      .subscribe(res => {
        // this.schoolList = res['data'];
        if (res.status == 200) {
          console.log('response school list', res);
          for (let i = 0; i < res.body['data'].length; i++) {
            let sr = {
              sr_no: i + 1
            }
            Object.assign(res.body['data'][i], sr);
          }
          console.log(res.body['data'])
          this.data = res.body['data'];
          this.filterData = res.body['data'];

          setTimeout(function () {
            this.table = $("#schoolTable").DataTable({
              dom: "Bfrtip",

              columns: [
                { width: "1%" },
                null,
                null,
                { width: "10%" },
                // null,null,null,null,null,null,
                { width: "5%" },
                { width: "8%" },
                { width: "12%" },
              ],
              aoColumnDefs: [
                {
                  bSortable: false,
                  aTargets: [-1]
                }
              ],
              retrieve: true,
              destroy: true
            });
            that.table = this.table;
          }, 200);
        }

      })
  }

  search(term: string) {
    if (!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
        x.School_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }

  updateUserStatus($userId){
    this.userService.updateUserStatus($userId)
        .subscribe(res => {
          if (res.status == 200 ) {
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.table.destroy();
            this.getSchoolList();
          }else{
            this.toastr.error(res.body['message'],'',{timeOut:600});;
          }
          console.log('response is here', res.body)
        })
  }
}
