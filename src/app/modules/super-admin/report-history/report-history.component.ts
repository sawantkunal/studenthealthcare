import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-report-history',
  templateUrl: './report-history.component.html',
  styleUrls: ['./report-history.component.css']
})
export class ReportHistoryComponent implements OnInit {

  userToken:any;
  User_id:any;
  userId:any;
  showStudentReport = [];
  month = []

  constructor(private userService:UserService, private active: ActivatedRoute) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    // console.log('userForm controls',this.f);
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      this.User_id = params['userid'];
    });
    this.showStudent();
  }

  showStudent(){
    this.userService.showStudentsHealth(this.userToken[0].Jwt_token,this.userToken[0].User_type, 1)
      .subscribe(res =>{
        res['data'].forEach(element =>{
          if(element.Created_at){
            var getMonth = {
              month:''
            }
            var month = element.Created_at.slice(5, 7);
            if(month == '01'){
              getMonth.month = 'Jan'
            }
            if(month == '02'){
              getMonth.month = 'Feb'
            }
            if(month == '03'){
              getMonth.month = 'Mar'
            }
            if(month == '04'){
              getMonth.month = 'Apr'
            }
            if(month == '05'){
              getMonth.month = 'May'
            }
            if(month == '06'){
              getMonth.month = 'June'
            }
            if(month == '07'){
              getMonth.month = 'July'
            }
            if(month == '08'){
              getMonth.month = 'Aug'
            }
            if(month == '09'){
              getMonth.month = 'Sep'
            }
            if(month == '10'){
              getMonth.month = 'Oct'
            }
            if(month == '11'){
              getMonth.month = 'Nov'
            }
            if(month == '12'){
              getMonth.month = 'Dec'
            }
            var a = Object.assign(element,getMonth);
            
            this.showStudentReport.push(a);

          }

        })
        console.log('shjow student list', this.showStudentReport);

      })
  }

}
