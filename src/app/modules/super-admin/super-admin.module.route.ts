import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AddSchoolComponent } from './add-school/add-school.component';
import { SchoolListComponent } from './school-list/school-list.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { StudentListComponent } from './student-list/student-list.component';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { StudentProfileComponent } from './student-profile/student-profile.component';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { ReportHistoryComponent } from '../../components/report-history/report-history.component';
import { AuthGuard } from '../../guards/auth.guard'; 
import { SchoolDetailsComponent } from './school-details/school-details.component'
import { LoginComponent } from '../../components/login/login.component';


export const routes:Routes=[
    { path: ' ', component: LoginComponent },
    { path: 'add_school', canActivate:[AuthGuard], component: AddSchoolComponent },
    { path: 'add_school/:id/:userid', canActivate:[AuthGuard], component: AddSchoolComponent },
    { path: 'school_list', canActivate:[AuthGuard], component: SchoolListComponent },
    { path: 'add_student', canActivate:[AuthGuard], component: AddStudentComponent },
    { path: 'add_student/:id/:userid', canActivate:[AuthGuard], component: AddStudentComponent },
    { path: 'student_list', canActivate:[AuthGuard], component: StudentListComponent },
    { path: 'add_doctor', canActivate:[AuthGuard], component: AddDoctorComponent },
    { path: 'add_doctor/:id/:userid', canActivate:[AuthGuard], component: AddDoctorComponent },
    { path: 'doctor_list', canActivate:[AuthGuard], component: DoctorListComponent },
    { path: 'student_profile/:id', canActivate:[AuthGuard], component: StudentProfileComponent },
    { path: 'doctor_profile/:id', canActivate:[AuthGuard], component: DoctorProfileComponent },
    { path: 'checkup_history', canActivate:[AuthGuard], component: ReportHistoryComponent },
    { path: 'school_details/:id', canActivate:[AuthGuard], component: SchoolDetailsComponent },
];

export const routing:ModuleWithProviders=RouterModule.forChild(routes);