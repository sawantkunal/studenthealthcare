import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-school-details',
  templateUrl: './school-details.component.html',
  styleUrls: ['./school-details.component.css']
})
export class SchoolDetailsComponent implements OnInit {

  userToken:any;
  userId:any;
  showSingleSchool:any;
  url:any;

  constructor(private active:ActivatedRoute,private router:Router, private userService:UserService,private _fb: FormBuilder) { }


  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      // console.log("ID",params['id']);
    });
    console.log('userForm csadasdontrols',this.userToken);
    // this.userService.getSchoolSingle(this.userToken[0]['Jwt_token'], this.userToken[0]['User_type'],this.userId,this.userId )
    this.userService.getSchoolSingle(this.userId )
      .subscribe(res =>{
        if(res.status == 200){
          this.showSingleSchool = res.body['data'][0];
          this.url = environment.url + "uploads/school-images/" ;
          console.log('this.addSchoolForm',this.showSingleSchool)

        }
      })
  }


  

}
