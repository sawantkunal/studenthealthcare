import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { environment } from '../../../../environments/environment';

declare let $;
@Component({
  selector: 'app-doctor-profile',
  templateUrl: './doctor-profile.component.html',
  styleUrls: ['./doctor-profile.component.css']
})
export class DoctorProfileComponent implements OnInit {

  userToken:any;
  userId:any;
  doctorData:any;
  url:any;

  constructor(private active:ActivatedRoute,private router:Router, private userService:UserService,private _fb: FormBuilder) { }

  ngOnInit() {
    // this.doctorFormData();
    // console.log('doctor form', this.f)
    // $(".js-example-basic-multiple").select2();
    // $('select').select2({
    //   placeholder: 'Click to choose school',
    //   disabled: 'true'
    // });

    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      console.log("ID",params['id']);
    });

    this.userService.getDoctorSingle(this.userId )
      .subscribe(res =>{
        if(res.status == 200){
          this.doctorData = res.body['data'][0];
          this.url = environment.url + "uploads/doctor-images/" ;
          console.log('this.url',this.url);
          // uploads/doctor-images/doctorData.profile_url
          console.log('Doctor details',this.doctorData)
        }
      })
  }


  // doctorFormData(){
  //   this.addDoctorForm = this._fb.group({
  //     First_name: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
  //     Email:['', Validators.compose([Validators.required, Validators.email])],
  //     Mobile:['',Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'),Validators.maxLength(10),Validators.minLength(10)])],
  //     Last_name:[''],
  //     School: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
  //     Specialist: ['', Validators.compose([Validators.required])],
  //     Assocated_hospital: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
  //     Designation: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
  //     Address: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
  //     Img_url: [''],
  //     Gender:[''],
  //   })
  // }
  // get f() { return this.addDoctorForm.controls }
  // get g() { return this.addDoctorForm.value}

}
