import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ValidationService } from '../../../services/validation.service';
import { ToastrService } from 'ngx-toastr';

declare let $;
@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.css']
})
export class AddDoctorComponent implements OnInit {

  addDoctorForm: FormGroup;
  formData:any;
  submitted = false;
  userToken:any;
  userId:any;
  User_id:any;
  file:any;
  stud_List:any;
  School=[];
  emailError:any;
  mobileError:any;
  disableButton:boolean;
  cities = [];
  dropdownSettings = {};

  constructor(private toastr: ToastrService,private active:ActivatedRoute, private router:Router, 
              private userService:UserService,private _fb: FormBuilder, private validationService: ValidationService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    // console.log('userForm controls',this.f);
    this.active.params.subscribe(params => {
      this.userId = params['id'];
    });

    this.doctorFormData(); 
    if(this.userId){
      this.userService.getDoctorSingle(this.userId)
        .subscribe(res =>{
          if(res.status == 200){
          console.log('response is getting', res.body['data']);
            this.addDoctorForm.patchValue(res.body['data'][0]);
          }
        })
    }
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'Id',
      textField: 'School_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 15,
      allowSearchFilter: true
    };

    // this.getStudentList();
  }
  onItemSelect(item: any) {
    // this.School.push(item.Id);
    this.School.push(item.School_name);
    console.log("item",item);
  }

  submitDoctor(){
    console.log('ssss')
    this.submitted = true;
    
   // debugger;
    if(this.addDoctorForm.valid) {
      this.submitted =  false
      if(this.emailError == 'false'){
        this.toastr.error('Your Email is Already Exit','',{timeOut:600});
      }else if (this.mobileError == 'false'){
        this.toastr.error('Your Mobile is Already Exit','',{timeOut:600});
      }
      else if(this.emailError && this.mobileError == 'true'){
        this.disableButton = true;
        console.log("this.addDoctorForm.value",this.addDoctorForm.value)
        var formData: any = new FormData()
            formData.append("file", this.file);
            formData.append('data', JSON.stringify(this.addDoctorForm.value))
            
        this.userService.addDoctor(formData)
          .subscribe(res =>{
            console.log('reposne dioctor', res);
            if(res.status == 200){
              this.toastr.success(res.body['message'],'',{timeOut:600});
              this.router.navigate(['doctor_list']);
            }
        })
      }
    }else{
      this.submitted = true;
      this.disableButton = false;
    }
   
  }

  updateDoctorForm(){
    console.log('this.addDoctorForm status',this.addDoctorForm.valid);
    if(this.addDoctorForm.valid) {
      if(this.emailError == "true"){

        this.toastr.error('Your Email is Already Exit','',{timeOut:600});
  
      }else if (this.mobileError == 'true'){
  
        this.toastr.error('Your Mobile is Already Exit','',{timeOut:600});
  
      }
      if(this.emailError == undefined || (this.emailError && this.mobileError == 'false')){
        this.disableButton = true;
        var formData: any = new FormData()
        formData.append("file", this.file);
        this.addDoctorForm.value.doctor_id = this.userId;
        formData.append('data', JSON.stringify(this.addDoctorForm.value))
        
        this.userService.updateDoctor(formData)
          .subscribe(response =>{
            console.log('response', response)
            if(response.status == 200){
              this.toastr.success(response.body['message'],'',{timeOut:600});
              this.router.navigate(['doctor_list']);
            }else{
              this.toastr.warning(response.body['message'],'',{timeOut:600});
            }
          })
      }
    }else{
      this.submitted =true;
      console.log('this.addDoctorForm.value false',this.addDoctorForm.valid);
    }
   //console.log('this.addDoctorForm.value',this.addDoctorForm.value);
    
  }

  checkEmail() {
    if(this.addDoctorForm.value.email){
      var email = {
        email: this.addDoctorForm.value.email
      }
      this.validationService.checkEmail(email)
        .subscribe(res => {
          if (res.body['status'] == 'false') {
            this.toastr.error(res.body['message'],'',{timeOut:600});
          }
          this.emailError = res.body['status'];
        })
    }
  }

  checkMobile() {
    var mobile = this.addDoctorForm.value.mobile;
    if(mobile){
      this.validationService.checkMobile(mobile)
        .subscribe(res => {
          if (res.body['status'] == 'false') {
            this.toastr.error(res.body['message'],'',{timeOut:600});
          }
          this.mobileError = res.body['status'];
        })
    }
  }
  
  handleChangeImage(event){
    this.file = event.target.files[0];

  }

  doctorFormData(){
    this.addDoctorForm = this._fb.group({

      fname: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      email:['', Validators.compose([Validators.required, Validators.email])],
      mobile:['',Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)])],
      lname:['',Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      // school: ['',Validators.compose([Validators.required])],
      specialist: ['', Validators.compose([Validators.required])],
      hospital_name: ['', Validators.compose([Validators.required])],
      designation: ['', Validators.compose([Validators.required])],
      address: [''],
      img_url: [''],
      doctor_img:[''],
      gender:['', Validators.compose([Validators.required])],
    })
  }
  get f() { return this.addDoctorForm.controls }
  get g() { return this.addDoctorForm.value}
  
  numberOnly(event){
   
    const charCode = (event.which) ? event.which : event.keyCode;
   // console.log('event',charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
