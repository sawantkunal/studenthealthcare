import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/user.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

declare let $;

@Component({
  selector: 'app-add-classes',
  templateUrl: './add-classes.component.html',
  styleUrls: ['./add-classes.component.css']
})
export class AddClassesComponent implements OnInit {
  // filterData: any;
  classList = [];
  ReadOnlyStyleGuideNotes: boolean = true;
  school_list: any;
  division_list = [];
  userId: any;
  addStudentForm: FormGroup;
  formData: any;
  submitted = false;
  submitted1 = false;
  division_id = null;
  disableButton = false;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  allClassDivision: any;
  classArray = [];
  selectedClass = []
  checkClassIndex: any;
  disabledClass: boolean = false;

  constructor(private router: Router, private userService: UserService,
    private toastr: ToastrService, private _fb: FormBuilder,
    private active: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.active.params.subscribe(params => {
      this.userId = params['id'];
      console.log("ID", params['id']);
    });
    this.getSchoolList();
    this.studentFormData();
    this.getAllClassDivision();
    if (this.userId) {
      this.userService.getClassDivisionBySchool(this.userId)
        .subscribe(res => {
          this.addStudentForm.controls.school_id.patchValue(this.userId);
          this.allClassDivision = res.body['data'];
          this.allClassDivision.forEach(element => {
            if (element.status == "true") {
              let classObj = { 'c_id': element.c_id }
              this.selectedClass.push(classObj);
            }
          })

        })
      // this.userService.getClassSingle(this.userId)
      //   .subscribe(res => {

      //     this.addStudentForm.patchValue(res.body['data'][0]);
      //     this.selectedItems = res.body['division'];
      //     console.log('this.selectedItems ', this.selectedItems)

      //   })
      this.getClass();
      // this.getDivision();

    }

    this.userService.getAllclass()
      .subscribe(res => {
        if (res.status == 200) {
          this.classList = res.body['data'];
        } else if (res.status == 202) {
          this.classList = [];
        }
      });
    this.dropdownList = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'c_id',
      textField: 'class_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 6,
      allowSearchFilter: true
    };

  }

  onItemSelect(item: any) {
    console.log(item);
    // console.log('sdajklsdfjklsdfjkl', this.selectedItems);
    // console.log('jjjjjjjjjjjjjj', this.addStudentForm.value)
  }
  onSelectAll(items: any) {
    console.log(items);
  }



  // addSchoolData() {
  //   this.submitted = true;

  //   console.log('this.addStudentForm.value', this.addStudentForm.value)
  //   var classIds = []
  //   for (let i = 0; i < this.addStudentForm.value.class_id.length; i++) {
  //     classIds.push(this.addStudentForm.value.class_id[i].c_id);
  //   }
  //   var classID = classIds.toString();
  //   console.log('divId', classID);
  //   this.addStudentForm.value.class = classID;

  //   // this.addStudentForm.value.division_id = 
  //   if (this.addStudentForm.valid) {
  //     this.submitted = false;
  //     this.disableButton = true;
  //     this.userService.addClass(this.addStudentForm.value)
  //       .subscribe(res => {
  //         if (res.status == 200) {
  //           this.toastr.success(res.body['message'], '', { timeOut: 600 });
  //           this.router.navigate(['classes_list']);
  //         } else {
  //           this.toastr.error(res.body['message'], '', { timeOut: 600 });
  //           this.disableButton = false;
  //         }
  //         console.log('response is here', res.body)
  //       })

  //   } else {
  //     this.disableButton = false;
  //   }
  // }

  addSchoolData() {
    this.submitted = true;
    if (this.addStudentForm.valid) {
      this.submitted = false;
      this.disableButton = true;
      if (this.selectedClass.length == 0) {
        this.toastr.error("Please select Class and Division");
        return false;
      }
      if (this.checkClassIndex == undefined) {
        this.toastr.error('Please Select Class ');
        return false;
      }
      this.addStudentForm.controls.classDivision.patchValue(this.allClassDivision)
      console.log(this.addStudentForm.value);
      // this.userService.addUpdateClassDivision(this.addStudentForm.value)
      //   .subscribe(res => {
      //     if (res.status == 200) {
      //       this.toastr.success(res.body['message'], '', { timeOut: 600 });
      //       this.router.navigate(['classes_list']);
      //     } else {
      //       this.toastr.error(res.body['message'], '', { timeOut: 600 });
      //       this.disableButton = false;
      //     }
      //   })
    }
  }
  updateSchoolData() {
    this.submitted = true;
    this.disableButton = true;
    this.addStudentForm.value.id = this.userId;

    var classIds = []
    for (let i = 0; i < this.addStudentForm.value.class_id.length; i++) {
      classIds.push(this.addStudentForm.value.class_id[i].c_id);
    }
    var classId = classIds.toString();
    console.log('divId', classId);
    this.addStudentForm.value.class = classId;

    if (this.addStudentForm.valid) {
      console.log('lskajdfkljlasdf', this.addStudentForm.value)

      this.submitted = false;
      this.userService.updateClass(this.addStudentForm.value)
        .subscribe(res => {
          if (res.status == 200) {
            this.toastr.success(res.body['message'], '', { timeOut: 600 });
            this.router.navigate(['classes_list']);
          }
          console.log('response is here', res.body)
        })

    }
  }

  getSchoolList() {
    this.userService.getSchool()
      .subscribe(res => {
        if (res.status == 200) {
          this.school_list = res.body['data'];
        }
        console.log('resposnse', res);
      })
  }

  getAllClassDivision() {
    this.userService.getAllClassDivision()
      .subscribe(res => {
        if (res.status == 200) {
          this.allClassDivision = res.body['data'];
        }
        console.log('resposnse', res);
      })
  }
  getClass() {

    //get school classes
    // this.userService.getClassBySchool(this.addStudentForm.value.school_id)
    //   .subscribe(res => {
    //     //     console.log('getClassBySchool',res.body['data'])
    //     this.selectedItems = res.body['data'];
    //     // res.body['data'].forEach(element => {
    //     //       if (typeof element.c_id  == 'string') {
    //     //         var cId = parseInt(element.c_id);
    //     //         var items = {
    //     //           c_id: cId,
    //     //           class_name: element.class_name,
    //     //         }
    //     //         this.selectedItems.push(items);
    //     //       }else{
    //     //         this.selectedItems.push(element);
    //     //       }
    //     //     });
    //     console.log('this.selectedItems', this.selectedItems)
    //     //   console.log('getClass kunal', res.body['data'])
    //   })
  }


  studentFormData() {
    this.addStudentForm = this._fb.group({
      school_id: ['', Validators.compose([Validators.required])],
      // class_id: ['', Validators.compose([Validators.required])],
      // division_id: ['', Validators.compose([Validators.required])],
      // division: [''],
      id: [''],
      class: [''],
      classDivision: ['']
    })
  }
  get f() { return this.addStudentForm.controls }
  get g() { return this.addStudentForm.value }

  selectClass(c_id, status) {
    let index = this.selectedClass.findIndex(obj => obj.c_id == c_id)

    if (index > -1) {
      this.selectedClass.splice(index, 1);
    }
    else {
      let classObj = { 'c_id': c_id }
      this.selectedClass.push(classObj);
    }

    let obj = this.allClassDivision.find(element => element.c_id == c_id);

    if (obj) {
      if (obj.c_id == c_id) {
        if (status == 'true') {
          obj.status = 'false';
        }
        else if (status == 'false') {
          obj.status = 'true';
        }
      }
    }

    var foundIndex = this.allClassDivision.findIndex(x => x.c_id == c_id);
    this.allClassDivision[foundIndex] = obj;

    console.log(this.allClassDivision)

  }

  selectDivision(c_id, division) {
    let classIndeax = this.selectedClass.find(element => element.c_id == c_id);
    this.checkClassIndex = classIndeax;
    if (classIndeax) {
      let obj = this.allClassDivision.find(element => element.c_id == c_id);

      if (obj) {
        if (obj.c_id == c_id) {
          let setDivision = obj.division;
          setDivision.map(element => {
            if (element.division_id == division.division_id) {
              if (division.status == 'true') {
                element.status = 'false';
                this.allClassDivision.find(v => v.c_id == c_id).division = setDivision;
              }
              else if (division.status == 'false') {
                element.status = 'true';
                this.allClassDivision.find(v => v.c_id == c_id).division = setDivision;
              }
            }
          })




        }
      }

    }
    else {
      this.toastr.error('Please Select Class ')
    }

  }

  checkDivision(id) {
    let obj = this.classArray.find(element => element.division.division_id == id);
    // debugger
    // if(obj){
    //   return true;
    // }
    // else{
    //   return false;
    // }

    return false;

  }

  addProp1(event) {
    this.disabledClass = true;
  }

  
}

