import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ValidationService } from '../../../services/validation.service';

declare let $;

@Component({
  selector: 'app-add-school',
  templateUrl: './add-school.component.html',
  styleUrls: ['./add-school.component.css']
})
export class AddSchoolComponent implements OnInit {

  addSchoolForm: FormGroup;
  formData: any;
  submitted = false;
  userToken: any;
  userId: any;
  User_id: any;
  fileName: any;
  emailError: any;
  mobileError: any;
  schoolError: any;
  school_id: number;
  disableButton:boolean;

  constructor(private router: Router, private toastr: ToastrService, private active: ActivatedRoute,
    private userService: UserService, private _fb: FormBuilder, private validationService: ValidationService,) { }

  ngOnInit() {
    this.userFormData();
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      // this.User_id = params['userid'];
      console.log("ID", params['id']);
    });
    if (this.userId) {
     
      this.userService.getSchoolSingle(this.userId)
        .subscribe(res => {
          console.log('serForm controls', res.body['data'])
          this.addSchoolForm.patchValue(res.body['data'][0]);
          this.school_id = res.body['data'][0]
        })
    }
    $('.select2').select2();
  }


  addSchoolFormData() {
    this.submitted = true;
    if (this.addSchoolForm.valid) {
      this.submitted = false;
      if (this.emailError == 'false') {
        this.toastr.error('Invalid Email','',{timeOut:600});
        return false;
      } else if (this.mobileError == 'false') {
        this.toastr.error('Your Mobile is Already Exit','',{timeOut:600});
        return false;
      } else if (this.emailError && this.mobileError == 'true') {
        console.log("this.addSchoolForm.value", this.addSchoolForm.value)
        var formData: any = new FormData()
        formData.append("file", this.fileName);
        formData.append('data', JSON.stringify(this.addSchoolForm.value))
        this.disableButton = true;
        this.userService.addSchool(formData)
          .subscribe(res => {
            if (res.status == 200 ) {
              this.toastr.success(res.body['message'],'',{timeOut:600});
              this.router.navigate(['school_list']);
            }
            else if (res.body && res.body['message'] && res.body['message'].website_url != null) {
              this.toastr.error(res.body['message'].website_url);

            }else{
              this.toastr.error(res.body['message'],'',{timeOut:600});
            }
            console.log('response is addSchool here', res.body)
          })
      }
    }
  }

  updateSchoolForm() {
    console.log('update school ', this.addSchoolForm.valid);
    this.submitted = true;
    if (this.addSchoolForm.valid) {
      
      this.submitted = false;
      if (this.emailError == "false") {

        this.toastr.error('Your Email is Already Exit','',{timeOut:600});

      } else if (this.mobileError == 'false') {

        this.toastr.error('Your Mobile is Already Exit','',{timeOut:600});

      } else if (this.schoolError == 'false') {
        this.toastr.error('School name os already exists','',{timeOut:600});
      }
      if (this.emailError == undefined || (this.emailError && this.mobileError == 'true')) {
       
        this.addSchoolForm.value.school_id = this.userId;
        this.disableButton = true;
        var formData: any = new FormData()
        formData.append("file", this.fileName);
        formData.append('data', JSON.stringify(this.addSchoolForm.value))

        this.userService.updateSchool(formData)
          .subscribe(res => {
            // if(res.body && res.body['message'].admin_name){
            //   this.toastr.error(res.body['message'].admin_name);
            // }
            if (res.body && res.body['message'] && res.body['message'].website_url != null) {
              this.toastr.error(res.body['message'].website_url[0],'',{timeOut:600});

            } else if (res.status == 200) {
              this.toastr.success(res.body['message'],'',{timeOut:600});
              this.router.navigate(['school_list']);
            }else{
              this.toastr.error(res.body['message'],'',{timeOut:600});
            }

             console.log('update response', res)

          })
      }
    }else{
      this.disableButton = false;
      console.log('update school formgfdgdgd', this.addSchoolForm.valid);
    }
  }

  checkEmail() {
    if(this.addSchoolForm.value.email){

    
    var email = {
      email: this.addSchoolForm.value.email
    }
    
    this.validationService.checkEmail(email)
      .subscribe(res => {
        if (res.body['status'] == 'false') {
          this.toastr.error(res.body['message'],'',{timeOut:600});
        }
        this.emailError = res.body['status'];
      })
    }
  }

  checkMobile() {
    var mobile = this.addSchoolForm.value.mobile
    if(mobile){
    this.validationService.checkMobile(mobile)
      .subscribe(res => {
        if (res.body['status'] == 'false') {
          this.toastr.error(res.body['message'],'',{timeOut:600});
        }
        this.mobileError = res.body['status'];
      })
    }
  }

  checkSchool() {
    var school_name = this.addSchoolForm.value.school_name
    if (!this.userId) {
      if(school_name){
        
      if (this.school_id && this.school_id['school_id'] != null) {
        let school = {
          school_id: this.school_id['school_id'],
          school_name: school_name
        }
        this.validationService.checkSchool(school)
          .subscribe(res => {
            if (res.body['status'] == 'false') {
              this.toastr.error(res.body['message'],'',{timeOut:600});
            }
            this.schoolError = res.body['status'];
          })
      } else {
        let school = {
          school_id: null,
          school_name: school_name
        }
        this.validationService.checkSchool(school)
          .subscribe(res => {
            if (res.body['status'] == 'false') {
              this.toastr.error(res.body['message'],'',{timeOut:600});
            }
            this.schoolError = res.body['status'];
          })
      }
      }
    }

  }

  handleChangeImage(event) {
    this.fileName = event.target.files[0];
    console.log('event handle', this.fileName);
  }

  userFormData() {
    const reg = '(https?://)?([\\dA-Za-z.-]+)\\.([A-Za-z.]{2,6})[/\\w .-]*/?';
    //, Validators.pattern("^[a-zA-Z +=._$']*$")
    this.addSchoolForm = this._fb.group({
      school_name: ['',  Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)])],
      First_name: [''],
      admin_name:['', Validators.compose([Validators.required])],
      website_url: ['', Validators.compose([Validators.required,  Validators.pattern(reg)])],
      // Class_from: [''],
      // Class_to: [''],
      address: ['',Validators.compose([Validators.required])],
      logo_url: ['']
    })
  }
  get f() { return this.addSchoolForm.controls; }
  get g() { return this.addSchoolForm }
  get website_url() { return this.addSchoolForm.get('website_url') };
  numberOnly(event){
   
    const charCode = (event.which) ? event.which : event.keyCode;
   // console.log('event',charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
