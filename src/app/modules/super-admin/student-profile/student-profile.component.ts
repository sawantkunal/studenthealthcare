import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.css']
})
export class StudentProfileComponent implements OnInit {

  addStudentForm: FormGroup;
  formData:any;
  submitted = false;
  userToken:any;
  userId:any;
  cities = [];
  studentData:any;
  url:any;

  constructor(private toastr: ToastrService,private active:ActivatedRoute, private userService:UserService,private _fb: FormBuilder) { }


  ngOnInit() {
    this.studentFormData();
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      // console.log("ID",params['id']);
    });
    // console.log('userForm controls',this.userToken);

    this.userService.getStudentSingle(this.userId )
    // this.userService.getSchoolSingle("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImVtYWlsIjoiYWRtaW4uYWRtaW4uY29tIiwicGFzc3dvcmQiOiIxMjM0NTYifX0.Wi8ansd3V0JVAD3Ul89cfF2RuuuodtCaSZ5sh1JWvw4", "3", "1")
      .subscribe(res =>{
        console.log('response', res)
        if(res.status == 200){
          this.studentData = res.body['data'][0];
          this.url = environment.url + "uploads/student_images/" ;
        }
        // this.addStudentForm.patchValue(res['data']);
        // debugger
        // this.addStudentForm.controls.student
        // if(res['data'].student !== null){
        //   this.addStudentForm.controls.student.patchValue(res['data'].student[0]);
        // }
        // if(res['data'].health !== null){
        //   this.addStudentForm.controls.health.patchValue(res['data'].health[0]);
        // }
        // this.addStudentForm.patchValue(res['data'].health);
        // console.log('this.addSchoolForm',this.addStudentForm.value)
        console.log('this.addSchoolForm',res['data'])
      })
  }

  studentFormData(){
    this.addStudentForm = this._fb.group({
      health: this._fb.group( {
        Address:  [''],
        Basal_etabolism:  [''],
        Bmi:  [''],
        Body_fat:  [''],
        Bone_mass:  [''],
        Bp:  [''],
        Disease:[''],
        Class:  [''],
        Created_at:  [''],
        Date_of_birth:  [''],
        Dental_report:  [''],
        Division:  [''],
        Eye_check:  [''],
        Father_mobile:  [''],
        Gender:  [''],
        Heart_rate:  [''],
        Height:  [''],
        Id:  [''],
        Mother_email:  [''],
        Mother_mobile:  [''],
        Mother_name:  [''],
        Muscle_mass:  [''],
        School:  [''],
        School_logo:  [''],
        Student_first:  [''],
        Student_id:  [''],
        Student_img:  [''],
        Student_last:  [''],
        Updated_at: [''],
        User_id: [''],
        Visceral_fat:[''],
        Water: [''],
        Weight: [''],
        blood_group: [''],
      }),
      student: this._fb.group( {
        Address:  [''],
        Basal_etabolism:  [''],
        Bmi:  [''],
        Body_fat:  [''],
        Bone_mass:  [''],
        Bp:  [''],
        Class:  [''],
        Created_at:  [''],
        Date_of_birth:  [''],
        Dental_report:  [''],
        Division:  [''],
        Eye_check:  [''],
        Father_mobile:  [''],
        Gender:  [''],
        Heart_rate:  [''],
        Height:  [''],
        Id:  [''],
        Mother_email:  [''],
        Mother_mobile:  [''],
        Mother_name:  [''],
        Muscle_mass:  [''],
        School:  [''],
        School_logo:  [''],
        Student_first:  [''],
        Student_id:  [''],
        Student_img:  [''],
        Student_last:  [''],
        Updated_at: [''],
        User_id: [''],
        Visceral_fat:[''],
        Water: [''],
        Weight: [''],
        blood_group: [''],
      })
      
    })
  }
  get f():any { return this.addStudentForm.controls; }
  get g() { return this.addStudentForm.value}
}
