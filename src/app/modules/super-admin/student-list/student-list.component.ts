import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

declare let $;

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  filterData: any;
  studentList: any;
  userToken: any;
  data: any;
  getStudent:any;
  getHealthCheckupDone:any;
  getHealthCheckupPending:any;
  table: any;
  token:any;

  constructor(private userService: UserService,private toastr: ToastrService,) { }

  ngOnInit() {
   
    this.userToken = JSON.parse(localStorage.getItem("user"));
    console.log('this.userToken',this.userToken.jwt_token)
    this.token = this.userToken.jwt_token
    this.getStudentList();
    this.getAllData();
  }

  getStudentList() {
    var that = this;
    this.userService.getStudentList()
      .subscribe(res => {
        console.log('response school list', res);

        if(res.status == 200){
          // console.log('response school list', res);
          for(let i=0; i< res.body['data'].length; i++){
            let sr = {
              sr_no: i+1
            }
            Object.assign(res.body['data'][i], sr);
          }
          this.data = res.body['data'];
          this.filterData = res.body['data'];
          setTimeout(function () {
            this.table = $("#studentTable").DataTable({
              dom: "Bfrtip",
                columns: [
                  { width: "1%" },
                  null,
                  null,
                  { width: "10%" },
                  // null,null,null,null,null,null,
                  { width: "5%" },
                  { width: "10%" },
                  { width: "5%" },
                  { width: "4%" },
                  { width: "4%" },
                  // null
                  { width: "12%" },
                ],
              aoColumnDefs: [
                {
                  bSortable: false,
                  aTargets: [-1]
                }
              ],
              retrieve: true,
              destroy: true
            });
            that.table = this.table;
          }, 200);
        }
     
      })
  }


  getAllData() {

    this.userService.getStudents()
      .subscribe(res => {
        // if (res['status'] == "true") {
        //   this.getStudent = res['data'];
        //   console.log('response', res);
        // }
      })

    // this.userService.getHealthCheckup(this.userToken.jwt_token,this.userToken.user_type_id,this.school_id)
    //   .subscribe(res =>{
    //     console.log('response from healthcheckup', res);
    //     this.getHealthCheckupDone = res['completed']
    //     this.getHealthCheckupPending = res['pending']
    //   })
  }

  search(term: string) {
    
    if (!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
        x.Student_first.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }

  updateStudentStatus($userId){
    console.log($userId);
    this.userService.updateUserStatus($userId)
        .subscribe(res => {
          if (res.status == 200 ) {
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.table.destroy();
            this.getStudentList();
          }else{
            this.toastr.error(res.body['message'],'',{timeOut:600});;
          }
          console.log('response is here', res.body)
        })
  }

}
