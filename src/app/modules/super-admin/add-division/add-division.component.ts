import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../services/user.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

declare let $;

@Component({
  selector: 'app-add-division',
  templateUrl: './add-division.component.html',
  styleUrls: ['./add-division.component.css']
})
export class AddDivisionComponent implements OnInit {

  classList= [];
  school_list: any;
  division_list = [];
  userId: any;
  addStudentForm: FormGroup;
  formData: any;
  submitted = false;
  submitted1 = false;
  division_id = null;
  disableButton:boolean = false;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  divisiondropdownSettings = {};
  classId:any;

  constructor(private router: Router, private userService: UserService,
    private toastr: ToastrService, private _fb: FormBuilder,
    private active: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.active.params.subscribe(params => {
      this.classId = params['id'];
      console.log("div  ID", params['id']);
    });
    this.getSchoolList();
    this.studentFormData();
    this.getDivision();

    if (this.classId) {
      this.userService.getClassDivision(this.classId)
        .subscribe(res => {
         
          this.addStudentForm.patchValue(res.body['data']);
          this.addStudentForm.value.class_id = res.body['data']['class_id'];
          this.getClass(res.body['data']['school_id']);
          this.getDivisionByClass(res.body['data']['class_id']);
      
        })
    }

    this.dropdownList = [];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'c_id',
      textField: 'class_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 6,
      allowSearchFilter: true
    };
    this.divisiondropdownSettings = {
      singleSelection: false,
      idField: 'division_id',
      textField: 'division_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 6,
      allowSearchFilter: true
    };
    
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }



  addSchoolData() {
    this.submitted = true;
    
    console.log('this.addStudentForm.value', this.addStudentForm.value)
    var div_id = []
    for (let i = 0; i < this.addStudentForm.value.division_id.length; i++) {
      div_id.push(this.addStudentForm.value.division_id[i].division_id);
    }
    var divId = div_id.toString();
    console.log('divId',divId);
    this.addStudentForm.value.division = divId;

    // this.addStudentForm.value.division_id = 
    if (this.addStudentForm.valid) {
      this.submitted = false;
      this.disableButton = true;
      this.userService.addClassDivision(this.addStudentForm.value)
        .subscribe(res => {
          if (res.status == 200 ) {
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.router.navigate(['classes_list']);
          }else{
            this.toastr.error(res.body['message'],'',{timeOut:600});
          }
          console.log('response is here', res.body)
        })

    }
  }

  updateSchoolData() {
    this.submitted = true;
    this.disableButton = true;
    this.addStudentForm.value.id = this.userId;

    var div_id = []
    for (let i = 0; i < this.addStudentForm.value.division_id.length; i++) {
      div_id.push(this.addStudentForm.value.division_id[i].division_id);
    }
    var divId = div_id.toString();
    console.log('divId',divId);
    this.addStudentForm.value.division = divId;

    if (this.addStudentForm.valid) {
      console.log('lskajdfkljlasdf', this.addStudentForm.value)

      this.submitted = false;
      this.userService.updateClass(this.addStudentForm.value)
        .subscribe(res => {
          if (res.status == 200 ) {
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.router.navigate(['classes_list']);
          }
          console.log('response is here', res.body)
        })

    }
  }

  getSchoolList() {
    this.userService.getSchool()
      .subscribe(res => {
        if (res.status == 200) {
          this.school_list = res.body['data'];
        }
        console.log('resposnse', res);
      })
  }

  getClass($schId) {
    this.userService.getClass($schId)
    .subscribe(res => {
      if (res.status == 200) {
        this.classList = res.body['data'];
      }else{
        this.classList = [];
      }
    });
  }
  getDivision() {
    
    this.userService.getAllDivision()
      .subscribe(res => {
        if(res.status == 200){
          this.division_list = res.body['data'];
          
        }else if(res.status == 202){
          this.division_list = [];
          this.toastr.error(res.body['message'],'',{timeOut:600});
        }
        
        console.log('getDivision', res.body['data'])
      })
  }

  getDivisionByClass($classId){
    this.userService.getDivision($classId)
      .subscribe(res => {
        if(res.status == 200){
          // this.division_list = res.body['data'];
          this.selectedItems = res.body['data'];
        }else if(res.status == 202){
          this.division_list = [];
        }
      });
  }

  getselectedDivision(event){
    console.log('event', event.target.value)
    this.getDivisionByClass(event.target.value)
  }
  studentFormData() {
    this.addStudentForm = this._fb.group({
      school_id: ['', Validators.compose([Validators.required])],
      class_id: ['', Validators.compose([Validators.required])],
      division_id: ['', Validators.compose([Validators.required])],
      division: [''],
      id:[''],
    })
  }
  get f() { return this.addStudentForm.controls }
  get g() { return this.addStudentForm.value }

}
