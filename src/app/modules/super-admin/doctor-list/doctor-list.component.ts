import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

declare let $;

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {

  doctorList:any;
  userToken:any;
  filterData:any;
  data:any;
  table: any;

  constructor( private userService: UserService,private toastr: ToastrService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"));
    this.getDocotrList();

  }

  getDocotrList(){
    var that = this;
    this.userService.getDoctorList()
      .subscribe(res =>{
        if(res.status == 200){
          for(let i=0; i< res.body['data'].length; i++){
            let sr = {
              sr_no: i+1
            }
            Object.assign(res.body['data'][i], sr);
          }
        }
        console.log('response doctor list', res);
        this.data = res.body['data'];
        this.filterData = res.body['data'];

        setTimeout(function() {
          this.table = $("#doctorTable").DataTable({
            dom: "Bfrtip",
            aoColumnDefs: [
              {
                bSortable: false,
                aTargets: [-1]
              }
            ],
            retrieve: true,
            destroy: true
          });
          that.table = this.table;
        }, 200);
      })
  }

  search(term: string) {
    if(!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
          x.First_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }

  updateUserStatus($userId){
    console.log($userId);
    this.userService.updateUserStatus($userId)
        .subscribe(res => {
          if (res.status == 200 ) {
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.table.destroy();
            this.getDocotrList();
          }else{
            this.toastr.error(res.body['message'],'',{timeOut:600});
          }
          console.log('response is here', res.body)
        })
  }
}
