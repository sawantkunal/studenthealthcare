import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  submitted = false;
  emailForm : FormGroup;

  getSchool:any;
  getStudent:any;
  getDoctor:any;
  getCheckup:any;
  userToken:any;

  constructor(private _fb: FormBuilder,private userService:UserService, private toastr: ToastrService) { }

  ngOnInit() {

    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.getAllData();
    this.emailMessage();

    console.log('controls', this.f)
  }

  sendMail(){
    this.submitted = true;
    if(this.emailForm.valid){
      this.userService.sendMail(this.userToken['jwt_token'], this.userToken['user_type_id'],this.emailForm.value.email,this.emailForm.value.message)
      .subscribe(response =>{
        if (response.status == 200) {
          this.emailForm.reset();
          this.submitted = false;
          this.toastr.success(response.body['message'],'',{timeOut:600});
        }else{
          this.toastr.error(response.body['message'],'',{timeOut:600});
        }
      })
    }else{
      console.log('status',this.emailForm.valid);
    }
    
  }

  getAllData(){
    this.userService.getSchools()
      .subscribe(res =>{
        if(res.status == 200){
          this.getSchool = res.body['data']
          console.log('getschool',this.getSchool)
        }
      })
    
    this.userService.getStudents()
      .subscribe(res =>{
        if(res.status == 200){
          this.getStudent = res.body['data']
        }
      })

    this.userService.getDoctors()
      .subscribe(res =>{
        if(res.status == 200){
          this.getDoctor = res.body['data']
        }
      })

    this.userService.getCheckups()
      .subscribe(res =>{

        if(res.status == 200){
          this.getCheckup = res.body['data']
        }
      })
  }
  chartOptions = {
    responsive: true
  };

  chartData = [
    { data: [800, 800, 800, 800], label: 'Total Students' },
    { data: [600, 755, 780, 400], label: 'Checkup Done' },
    { data: [200, 45, 20, 400], label: 'Checkup Pending' }
  ];

  // chartLabels = ['Total Students', 'Checkup Done', 'Checkup Pending'];
  chartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];

  onChartClick(event) {
    console.log(event);
  }
  
  emailMessage(){
    this.emailForm = this._fb.group({
      message: ['', Validators.compose([Validators.required])],
      email:['', Validators.compose([Validators.required, Validators.email])],
    })
  }
  get f() { return this.emailForm.controls }
  get g() { return this.emailForm.value}
  
}
