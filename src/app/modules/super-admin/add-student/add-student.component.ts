import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ValidationService } from '../../../services/validation.service';
import { IfStmt } from '@angular/compiler';


@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {


  addStudentForm: FormGroup;
  formData: any;
  submitted = false;
  userToken: any;
  userId: number;
  User_id: any;
  cities = [];
  file: any;
  file1: any;
  emailError: any;
  mobileError: any;
  school_list: any;
  classList: any;
  emailList: any;
  BGList: any;
  email: any;
  selectFlag = false;
  parentFlag = false;
  parentResponseData: any;
  division_list =[];
  disableButton:boolean;
  constructor(private router: Router, private toastr: ToastrService, private active: ActivatedRoute,
    private userService: UserService, private _fb: FormBuilder, private validationService: ValidationService) { }


  ngOnInit() {
    this.studentFormData();
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      this.User_id = params['userid']

    });

    if (this.userId) {

      this.userService.getStudentSingle(this.userId)
        .subscribe(res => {
          if (res.status == 200) {
            this.addStudentForm.patchValue(res.body['data'][0]);

            console.log('school Id', res.body['data'][0]['school_id'])

            this.userService.getClass(res.body['data'][0]['school_id'])
              .subscribe(res => {
                this.classList = res.body['data'];
                console.log('getClass', res.body['data'])
              })
              console.log('this.addStudentForm.value.division_id',this.addStudentForm.value.division_id)
              this.userService.getDivision(this.addStudentForm.value.class_id)
              .subscribe(res => {
                if(res.status == 200){
                  this.division_list = res.body['data'];
                  console.log('this.division_list',res);
                  
                }else if(res.status == 202){
                  this.division_list = [];
                  this.toastr.error(res.body['message'],'',{timeOut:600});
                }
                
                console.log('getDivision', res.body['data'])
              })
          }
        })

        
    }
    console.log('asdjkl;sdajkl', this.f);
    this.getSchoolList();
    this.getBloodGroup();
  }

  checkParentExist() {
    this.selectFlag = true;
    this.userService.getParentEmails(this.addStudentForm.value.email)
      .subscribe(res => {
        if (res.body['data']) {
          this.emailList = res.body['data'];
          this.selectFlag = true;
        }
        console.log('getParents', this.emailList[0])
      })

  }

  removeDisable(event) {
    if (event.target.value && this.emailList.length == 0) {
      this.parentFlag = false;
    }
  }

  selectEmail(email) {
    this.email = email;
    this.selectFlag = false;
    this.userService.getParentDetails(this.email)
      .subscribe(res => {
        if (res.body['data']) {
          this.parentFlag = true;
          this.parentResponseData = res.body['data'];
          this.emailError = true;
          this.mobileError = true;
          console.log('getParents', this.parentResponseData)

        } else {
          this.parentFlag = false;
          this.parentResponseData = null;
          console.log('this.parentFlag', this.parentFlag);
        }
      })

  }

  handleChangeImage(event) {
    this.file = event.target.files[0]
    console.log('event handle', event.target.files)
  }

  handleChangeImage1(e) {
    this.file1 = e.target.files[0]

    console.log('event handle1', e.target.files)
  }

  submitStudent() {

    // debugger;
    if(this.division_list.length == 0){
      this.addStudentForm.controls['division_id'].clearValidators();
      this.addStudentForm.controls['division_id'].updateValueAndValidity();
    }else{
      this.addStudentForm.controls['division_id'].setValidators([Validators.required]);
      this.addStudentForm.controls['division_id'].updateValueAndValidity();
    }
   
    console.log('flags', this.selectFlag, this.parentFlag)
    if (this.parentFlag == false) {
      this.emailError = true;
      this.mobileError = true;
    }
    this.submitted = true;
    
    console.log('foram valid success', this.addStudentForm.value, this.addStudentForm.valid);
    if (this.addStudentForm.valid) {
      console.log('error', this.emailError, this.mobileError)
      this.submitted = false;
      this.disableButton = true;
      if (this.emailError == 'false') {

        this.toastr.error('Invalid Email.','',{timeOut:600});

      } else if (this.mobileError == 'false') {

        this.toastr.error('Your Mobile is Already Exist','',{timeOut:600});

      }
      if (this.emailError == true && this.mobileError == true) {
        console.log("this.addSchoolForm.value", this.g)
        var formData: any = new FormData()
        formData.append("Student_img", this.file);
        // this.formData.append("School_logo", this.file1);
        formData.append('data', JSON.stringify(this.addStudentForm.value))

        this.userService.addStudent(formData)
          .subscribe(res => {
            if (res.status == 200) {
              this.toastr.success(res.body['message'],'',{timeOut:600});
              this.router.navigate(['student_list']);
            } else if (res.status == 202) {
              this.toastr.error(res.body['message'],'',{timeOut:600});
            }
            console.log('response is here', res.body)
          })
      }
    }else{
      this.submitted =true;
    }



  }
  getToday(): string {
    return new Date().toISOString().split('T')[0]
  }
  getSchoolList() {
    this.userService.getSchool()
      .subscribe(res => {
        if (res.status == 200) {
          this.school_list = res.body['data'];
        }
        console.log('getSchoolList', res);
      })
  }

  getBloodGroup() {
    this.userService.getBloodGroup()
      .subscribe(res => {
        if (res.status == 200) {
          this.BGList = res.body['data'];
        }
        console.log('getBloodGroup', res)
      })
  }


  updateStudentForm() {
    // console.log(' this.addStudentForm.value.class_id ', this.addStudentForm.value.class_id );
    this.submitted = true;
    this.disableButton = true;
    if( this.addStudentForm.valid){
      if (this.emailError == 'false') {
        this.toastr.error('Your Email is Already Exit','',{timeOut:600});
  
      } else if (this.mobileError == 'false') {
  
        this.toastr.error('Your Mobile is Already Exit','',{timeOut:600});
  
      }
      if (this.emailError == undefined || (this.emailError && this.mobileError == 'false')) {

        this.addStudentForm.value.id = this.userId;
  
        var formData: any = new FormData()
        formData.append("Student_img", this.file);
        formData.append('data', JSON.stringify(this.addStudentForm.value));
        this.submitted = false;
        this.userService.updateStudent(formData)
          .subscribe(response => {
            if (response.status == 200) {
              this.toastr.success(response.body['message'],'',{timeOut:600});
              this.router.navigate(['student_list']);
            }else{
              this.toastr.error(response.body['message'],'',{timeOut:600});
            }
            console.log('response', response)
          })
      }
    }else{
      this.submitted = true;
    }
  }

  checkEmail() {
    // this.selectFlag = false;
    // console.log("selectFlag, parentFlag",this.selectFlag,this.parentFlag)

    // if(this.selectFlag && this.parentFlag==false){
    //   console.log('No email validation');
    // }else{
    //   console.log('do email validation')
    // }
    // var email = {
    //   email: this.addStudentForm.value.email
    // }
    // this.validationService.checkEmail(email)
    //   .subscribe(res => {
    //     if (res.body['status'] == 'false') {
    //       this.toastr.error(res.body['message'],'',{timeOut:600});;
    //     }
    //     this.emailError = res.body['status'];
    //   })
  }

  checkMobile() {
    var mobile = this.addStudentForm.value.mobile
    this.validationService.checkMobile(mobile)
      .subscribe(res => {
        if (res.body['status'] == 'false') {
          this.toastr.error(res.body['message'],'',{timeOut:600});
        }
        this.mobileError = res.body['status'];
      })
  }

  getClass(event) {
    this.classList = null;
    this.addStudentForm.value.class_id = null;
    this.addStudentForm.value.division_id = null;
    this.division_list = [];
    console.log('school_id', event.target.value)
    this.userService.getClass(event.target.value)
      .subscribe(res => {
        if(res.body && res.body['data'].length == 0){
          this.toastr.warning("There is no more classes for this school",'',{timeOut:600});
          this.classList = res.body['data'];
        }else if(res.body && res.body['data'].length > 0){
          this.classList = res.body['data'];
          console.log('getClass', res.body['data'])
        }
      })
  }
  getDivision(event) {
    this.division_list = [];
    console.log('getDivision getDivision',event.target.value);
    this.userService.getDivision(event.target.value)
      .subscribe(res => {
        if(res.status == 200){
          this.division_list = res.body['data'];
          
        }else if(res.status == 202){
          this.division_list = [];
          this.toastr.error(res.body['message'],'',{timeOut:600});
        }
        
        console.log('getDivision', res.body['data'])
      })
  }
  studentFormData() {
    this.addStudentForm = this._fb.group({
      parent_id: [''],
      s_lname: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      s_fname: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      mobile: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)])],
      email: ['', Validators.compose([Validators.email, Validators.required, Validators.pattern('[a-zA-Z0-9.-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{1,}')])],
      // mother_email: ['', Validators.compose([Validators.required, Validators.email])],
      dob: ['', Validators.compose([Validators.required])],
      class_id: ['', Validators.compose([Validators.required])],
      division_id: ['', Validators.compose([Validators.required])],
      father_name: ['', Validators.compose([Validators.required])],
      // first_name: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      // Father_email: ['', Validators.compose([Validators.required, Validators.email])],
      father_mobile: ['', Validators.compose([Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)])],
      mother_mobile: ['', Validators.compose([ Validators.pattern('^[0-9]*$'), Validators.maxLength(10), Validators.minLength(10)])],
      mother_name: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      school_id: ['', Validators.compose([Validators.required])],
      address: [''],
      profile_img: [''],
      blood_group: [''],
      allergies: [''],
      // weight:[''],
      // bp:['']

    })
  }
  get f(): any { return this.addStudentForm.controls; }
  get g() { return this.addStudentForm.value }

  numberOnly(event) {

    const charCode = (event.which) ? event.which : event.keyCode;
    // console.log('event',charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
