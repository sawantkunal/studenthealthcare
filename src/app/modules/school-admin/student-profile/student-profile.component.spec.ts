import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentProfileComponents } from './student-profile.component';

describe('StudentProfileComponent', () => {
  let component: StudentProfileComponents;
  let fixture: ComponentFixture<StudentProfileComponents>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentProfileComponents ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentProfileComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
