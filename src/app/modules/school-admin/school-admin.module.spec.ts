import { SchoolAdminModule } from './school-admin.module';

describe('SchoolAdminModule', () => {
  let schoolAdminModule: SchoolAdminModule;

  beforeEach(() => {
    schoolAdminModule = new SchoolAdminModule();
  });

  it('should create an instance', () => {
    expect(schoolAdminModule).toBeTruthy();
  });
});
