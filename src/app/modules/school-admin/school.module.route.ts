import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


// import { SchoolAdminComponent } from './school-admin/school-admin.component';
// import { AuthGuard } from '../../guards/auth.guard';
import { LoginComponent } from '../../components/login/login.component';


export const routes:Routes=[
    { path: '', component: LoginComponent },
    // { path: 'school_admin', canActivate :[AuthGuard], component: SchoolAdminComponent },
];

export const routing:ModuleWithProviders=RouterModule.forChild(routes);