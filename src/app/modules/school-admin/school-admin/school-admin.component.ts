import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-school-admin',
  templateUrl: './school-admin.component.html',
  styleUrls: ['./school-admin.component.css']
})
export class SchoolAdminComponent implements OnInit {

  getStudent:any;
  getHealthCheckupDone:any;
  getHealthCheckupPending:any;
  userToken:any;
  userId:any;
  getSchool:any;
  SchoolId:any;


  constructor(private userService:UserService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"));
    this.userId = this.userToken['user_id'];
    this.SchoolId = this.userToken['school_id'];
    console.log('this.SchoolId',this.SchoolId);
    this.getAllData();
  }

  getAllData() {

    // this.userService.getStudents()
    //   .subscribe(res => {
    //     console.log('response', res);
    //     // if (res['status'] == "true") {
    //     //   this.getStudent = res['data'];
    //     //   // console.log('response', res);
    //     // }
    //   })

    
    
    

    this.userService.getSchoolId(this.userId)
    .subscribe(res =>{
      if(res.status == 200){
        this.SchoolId = res.body['data'];
       // console.log('SchoolId',this.SchoolId);

        this.userService.getSchoolSingle(this.SchoolId)
        .subscribe(res =>{
          if(res.status == 200 && res.body['data']){
            this.getSchool = res.body['data'][0];
            console.log('response getSchool',this.getSchool);
            this.SchoolId = this.getSchool.school_id;
            console.log('this.schoolid',this.SchoolId)
            this.userService.getSchoolStudentCount(this.SchoolId)
            .subscribe(res =>{
              if(res.status == 200){
                this.getStudent = res.body['data']
              }
              console.log('getSchoolStudentCount',res.body['data']);
            })
          }
        })

        this.userService.getHealthCheckup(this.userToken.jwt_token,this.userToken.user_type_id,this.SchoolId)
        .subscribe(res =>{
          // console.log('response from healthcheckup', res);
          this.getHealthCheckupDone = res['completed']
          this.getHealthCheckupPending = res['pending']
        })

      }
    })
    
  }


}
