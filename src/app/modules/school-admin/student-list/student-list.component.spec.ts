import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentListComponents } from './student-list.component';

describe('StudentListComponent', () => {
  let component: StudentListComponents;
  let fixture: ComponentFixture<StudentListComponents>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentListComponents ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentListComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
