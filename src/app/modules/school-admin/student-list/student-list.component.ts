import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';

declare let $;

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponents implements OnInit {

  filterData: any;
  studentList: any;
  userToken: any;
  data: any;
  getStudent:any;
  getHealthCheckupDone:any;
  getHealthCheckupPending:any;
  token:any;
  usertype:any;
  SchoolId:any;
  userId:any;
  table:any;

  constructor(private userService: UserService) { }

  ngOnInit() {
    // $('#example1').DataTable();
    this.userToken = JSON.parse(localStorage.getItem("user"));
    this.userId = this.userToken['user_id'];
    
    this.getStudentList();
    // console.log('here oninit')
    this.getAllData();
  }

  getStudentList() {
    var that = this;
    this.userService.getSchoolId(this.userId)
    .subscribe(res =>{
      if(res.status == 200){
        this.SchoolId = res.body['data'];
        
        var token = this.userToken.jwt_token;
        var usertype = this.userToken.user_type_id;
       if(this.SchoolId){
         console.log('this.SchoolId',this.SchoolId);
         this.userService.getStudentBySchoolId(token, usertype, this.SchoolId)
         .subscribe(res => {
           console.log('response school list', res);
   
           if(res.status == 200){
             // console.log('response school list', res);
             for(let i=0; i< res.body['data'].length; i++){
               let sr = {
                 sr_no: i+1
               }
               Object.assign(res.body['data'][i], sr);
             }
             this.data = res.body['data'];
             this.filterData = res.body['data'];

             setTimeout(function() {
              this.table = $("#studentTable").DataTable({
                dom: "Bfrtip",
                aoColumnDefs: [
                  {
                    bSortable: false,
                    aTargets: [-1]
                  }
                ],
                retrieve: true,
                destroy: true
              });
              that.table = this.table;
            }, 200);
           }

          //  this.userService.getHealthCheckup(this.userToken.jwt_token,this.userToken.user_type_id,this.SchoolId)
          //   .subscribe(res =>{
          //     console.log('response from healthcheckup', res);
          //     this.getHealthCheckupDone = res['completed']
          //     this.getHealthCheckupPending = res['pending']
          //   })
        
         })
       }
       
      }
    })

    
  }


  getAllData() {

    this.userService.getStudents()
      .subscribe(res => {
        // if (res['status'] == "true") {
        //   this.getStudent = res['data'];
        //   console.log('response', res);
        // }
      })

    
  }

  search(term: string) {
    if (!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
        x.Student_first.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }

}
