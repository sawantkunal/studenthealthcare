import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchoolAdminComponent } from './school-admin/school-admin.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {DataTableModule} from "angular-6-datatable";

// import { routing } from './school.module.route';

@NgModule({
  imports: [
    CommonModule,
    // routing,
    DataTableModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    // SchoolAdminComponent
  ],
  exports: [
    // SchoolAdminComponent,
    // NgMultiSelectDropDownModule.forRoot()
  ]
})
export class SchoolAdminModule { }
