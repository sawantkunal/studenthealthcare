import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userToken:any;
  userId:any;
  constructor(private _fb: FormBuilder,private userService:UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.userId = this.userToken['user_id'];
    this.getDoctorData();
  }

  getDoctorData(){
    this.userService.getDoctorData(this.userId)
      .subscribe(response =>{
        if (response.status == 200) {
          console.log('getDoctorData',response);
          // this.toastr.success(response.body['message'],'',{timeOut:600});
        }else{
          // this.toastr.error(response.body['message'],'',{timeOut:600});
        }
      })
  }
}
