import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastassignedschoolListComponent } from './pastassignedschool-list.component';

describe('PastassignedschoolListComponent', () => {
  let component: PastassignedschoolListComponent;
  let fixture: ComponentFixture<PastassignedschoolListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastassignedschoolListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastassignedschoolListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
