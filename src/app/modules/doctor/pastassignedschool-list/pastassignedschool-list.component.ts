import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

declare let $;

@Component({
  selector: 'app-pastassignedschool-list',
  templateUrl: './pastassignedschool-list.component.html',
  styleUrls: ['./pastassignedschool-list.component.css']
})
export class PastassignedschoolListComponent implements OnInit {

  dropdownSettings: any;
  userToken: any;
  User_id: any;
  userId: any;
  schId: any;
  school_List: any;
  filterData = [];
  data = [];
  getHealthCheckupDone: any;
  getHealthCheckupPending: any;
  getStudent: any;
  table :any;

  constructor(private active: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
   
    this.userToken = JSON.parse(localStorage.getItem("user"))
    console.log('userForm this.userToken', this.userToken);
    this.active.params.subscribe(params => {
      this.schId = params['id'];
      
    });

    if(this.schId){
      
      this.userService.getStudentData(this.schId)
        .subscribe(res =>{
          if(res.status == 200){
          console.log('response is getting', res.body);
            //this.addDoctorForm.patchValue(res.body['data'][0]);
          }
          else if(res.status == 202){
            console.log('response is getting', res);
          }
        })
    }
    
    this.getSchoolList();
  }


  getSchoolList() {
    var that = this;
    // this.userService.getPastDoctorSchool(this.userToken.jwt_token, this.userToken.user_type_id, this.userToken.user_id)
    this.userService.getpastdoctorschool(this.userToken.user_id)
      .subscribe(res => {
        console.log('response getPastDoctorSchool',res);
        if (res.status == 200 && res.body['data'] != null) {
        
          for(let i=0; i< res.body['data'].length; i++){
            let sr = {
              sr_no: i+1
            }
            Object.assign(res.body['data'][i], sr);
          }
          res.body['data'].forEach(element => {
            this.filterData.push(element);
            this.data.push(element);
          });
          setTimeout(function() {
            this.table = $("#padsTable").DataTable({
              dom: "Bfrtip",
              aoColumnDefs: [
                {
                  bSortable: false,
                  aTargets: [-1]
                }
              ],
              retrieve: true,
              destroy: true
            });
            that.table = this.table;
          }, 200);
        }

      })
  }


  onItemSelect(event) {
    // this.School.push(item.Id);
    // this.School.push(item.School_name);
    // console.log("item",event.target.value);
  }


  search(term: string) {
    if (!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
        x.School_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }

}
