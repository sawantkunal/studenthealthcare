import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute,NavigationEnd,NavigationStart, Event } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { element } from '@angular/core/src/render3/instructions';

declare let $;
@Component({
  selector: 'app-student-report',
  templateUrl: './student-report.component.html',
  styleUrls: ['./student-report.component.css']
})
export class StudentReportComponent implements OnInit {

  dropdownSettings:any;
  userToken:any;
  User_id:any;
  userId:any;
  school_List:any;
  sname:any;
  data = [];
  filterData = []

  constructor(private active:ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    // $('#example1').DataTable();
    this.userToken = JSON.parse(localStorage.getItem("user"))
    // console.log('userForm controls',this.f);
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      this.User_id = params['userid'];
    });

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'Id',
      textField: 'School_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 15,
      allowSearchFilter: true
    };

    this.getStudentList();
    this.getStudentHealthBySchool();
  }


  getStudentSingle(){
    this.userService.getStudentSingle(this.userId)
      .subscribe(res =>{
        console.log('resoibnse single user', res);
      })
  }
 onItemSelect(item) {
    // this.School.push(item.Id);
    // this.School.push(item.School_name);
    console.log("item",item.target.value);
    console.log("item",item.target);
  }

  getStudentHealthBySchool(){
    this.userService.getStudentHealthBySchool(this.userToken[0].Jwt_token,this.userToken[0].User_type,this.userToken[0].Id)
      .subscribe(res =>{
        res['data'].forEach(element =>{
          element.forEach(ele =>{
            this.data.push(ele);
            this.filterData.push(ele);
            console.log('school_List this.studentHealthList', this.data);

          })
        })
      })
  }
  getStudentList(){
    this.userService.getSchool()
    .subscribe(res =>{
      this.school_List = res.body['data'];
      console.log('studentHealthList', this.school_List);

    })
  }

  search(term: string) {
    if(!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
          x.Student_first.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }
}
