import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorAssignedSchoolsComponent } from './doctor-assigned-schools.component';

describe('DoctorAssignedSchoolsComponent', () => {
  let component: DoctorAssignedSchoolsComponent;
  let fixture: ComponentFixture<DoctorAssignedSchoolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorAssignedSchoolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorAssignedSchoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
