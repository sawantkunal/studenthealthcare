import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaststudentcheckupComponent } from './paststudentcheckup.component';

describe('PaststudentcheckupComponent', () => {
  let component: PaststudentcheckupComponent;
  let fixture: ComponentFixture<PaststudentcheckupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaststudentcheckupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaststudentcheckupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
