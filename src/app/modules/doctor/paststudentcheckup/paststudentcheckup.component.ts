import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-paststudentcheckup',
  templateUrl: './paststudentcheckup.component.html',
  styleUrls: ['./paststudentcheckup.component.css']
})
export class PaststudentcheckupComponent implements OnInit {

  addStudentCheckupForm: FormGroup;
  formData:any;
  submitted = false;
  userToken:any;
  sname:any;
  User_id:any;
  userId:any;
  checkupId:any;
  studCheckup:any;
  cities = [];
  Student_id:any;
  studentData:any;
  sch_checkupId:any;

  constructor(private toastr: ToastrService,private active:ActivatedRoute, private router:Router, private userService:UserService,private _fb: FormBuilder) { }


  ngOnInit() {
    this.studentCheckUpFormData();
    console.log('conterols', this.f);
    
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.active.params.subscribe(params => {
       this.checkupId = params['id'];
       console.log("checkup ID",this.checkupId);
    });
    
    if(this.checkupId){
      this.userService.getCheckupDetail(this.checkupId)
        .subscribe(res =>{
          if(res.status == 200){
          console.log('response is getting', res.body['data'][0]);
            this.addStudentCheckupForm.patchValue(res.body['data'][0]);

            this.studentData = res.body['data'][0];
            this.sch_checkupId = res.body['data'][0]['scheduled_checkup_id'];
          }
          else if(res.status == 202){
            console.log('response is getting', res);
          }
        })
    }
  }

  studentCheckUpFormData(){
    this.addStudentCheckupForm = this._fb.group({
      Student_id:[''],
      spherical_left:[''],
      spherical_right:[''],
      cylindrical_left:[''],
      cylindrical_right:[''],
      axis_left:[''],
      axis_right:[''],
      ap_left:[''],
      ap_right:[''],
      height:[''],
      weight:[''],
      bmi:[''],
      bp:[''],
      heart_rate:[''],
      muscle_mass:[''],
      body_fat:[''],
      water:[''],
      basal_metabolism:[''],
      bone_mass:[''],
      dental_report:[''],
      consultation_notes:[''],
      visceral_fat:[''],
      suggested_treatment:[''],
    })
  }
  get f() { return this.addStudentCheckupForm.controls; }
  get g() { return this.addStudentCheckupForm.value}

}
