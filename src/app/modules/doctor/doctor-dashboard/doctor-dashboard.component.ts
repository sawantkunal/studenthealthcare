import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-doctor-dashboard',
  templateUrl: './doctor-dashboard.component.html',
  styleUrls: ['./doctor-dashboard.component.css']
})
export class DoctorDashboardComponent implements OnInit {
  userToken:any;
  userId:any;
  total:any;
  upcoming:any;
  completed:any;

  constructor(private _fb: FormBuilder,private userService:UserService, private toastr: ToastrService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.userId = this.userToken['user_id'];
    this.getDoctorData();
  }
  getDoctorData(){
    this.userService.getDoctorData(this.userId)
      .subscribe(response =>{
        if (response.status == 200) {
          console.log('getDoctorData',response);
          this.total = response.body['totalCheckups']
          this.upcoming = response.body['upcoming']
          this.completed = response.body['completed']
          // this.toastr.success(response.body['message'],'',{timeOut:600});
        }else{
          // this.toastr.error(response.body['message'],'',{timeOut:600});
        }
      })
  }
}
