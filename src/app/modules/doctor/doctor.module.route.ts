import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { DoctorAssignedSchoolsComponent } from './doctor-assigned-schools/doctor-assigned-schools.component';
import { StudentReportComponent } from './student-report/student-report.component';
import { StudentCheckupComponent } from './student-checkup/student-checkup.component';
import { LoginComponent } from '../../components/login/login.component';

import { AuthGuard } from '../../guards/auth.guard'; 

export const routes:Routes=[
    {path:'', component:LoginComponent},
    { path: 'doctor_assigned_school', canActivate:[AuthGuard], component: DoctorAssignedSchoolsComponent },
    { path: 'student_checkup',  canActivate:[AuthGuard], component: StudentCheckupComponent },
    { path: 'student_checkup/:id/:userId/:sname',  canActivate:[AuthGuard], component: StudentCheckupComponent },
    { path: 'student_report',  canActivate:[AuthGuard], component: StudentReportComponent },
];

export const routing:ModuleWithProviders=RouterModule.forChild(routes);