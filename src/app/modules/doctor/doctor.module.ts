import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorAssignedSchoolsComponent } from './doctor-assigned-schools/doctor-assigned-schools.component';
import { StudentReportComponent } from './student-report/student-report.component';
import { StudentCheckupComponent } from './student-checkup/student-checkup.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// import { routing } from './doctor.module.route'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {DataTableModule} from "angular-6-datatable";
import { StudentListComponent } from './student-list/student-list.component';
import { SchoolstudentListComponent } from './schoolstudent-list/schoolstudent-list.component';
import { PastassignedschoolListComponent } from './pastassignedschool-list/pastassignedschool-list.component';
import { PastschoolstudentListComponent } from './pastschoolstudent-list/pastschoolstudent-list.component';
import { PaststudentcheckupComponent } from './paststudentcheckup/paststudentcheckup.component';
import { StudentCheckupDetailComponent } from './student-checkup-detail/student-checkup-detail.component';
import { TodaysBookingsComponent } from './todays-bookings/todays-bookings.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  imports: [
    CommonModule,
    // routing,
    ReactiveFormsModule,
    FormsModule,
    DataTableModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    DoctorAssignedSchoolsComponent,
    StudentReportComponent,
    StudentCheckupComponent,
    StudentListComponent,
    SchoolstudentListComponent,
    PastassignedschoolListComponent,
    PastschoolstudentListComponent,
    PaststudentcheckupComponent,
    StudentCheckupDetailComponent,
    TodaysBookingsComponent,
    DashboardComponent
  ],
  exports: [
    DoctorAssignedSchoolsComponent,
    StudentReportComponent,
    StudentCheckupComponent,
    TodaysBookingsComponent,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class DoctorModule { }
