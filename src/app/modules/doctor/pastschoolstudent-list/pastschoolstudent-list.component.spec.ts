import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastschoolstudentListComponent } from './pastschoolstudent-list.component';

describe('PastschoolstudentListComponent', () => {
  let component: PastschoolstudentListComponent;
  let fixture: ComponentFixture<PastschoolstudentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastschoolstudentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastschoolstudentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
