import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolstudentListComponent } from './schoolstudent-list.component';

describe('SchoolstudentListComponent', () => {
  let component: SchoolstudentListComponent;
  let fixture: ComponentFixture<SchoolstudentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolstudentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolstudentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
