import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

declare let $;

@Component({
  selector: 'app-schoolstudent-list',
  templateUrl: './schoolstudent-list.component.html',
  styleUrls: ['./schoolstudent-list.component.css']
})
export class SchoolstudentListComponent implements OnInit {

  schId: any;
  userToken: any;
  filterData = [];
  data = [];
  table :any;

  constructor(private active: ActivatedRoute, private userService: UserService) { }
  ngOnInit() {

    this.userToken = JSON.parse(localStorage.getItem("user"))
    console.log('userForm this.userToken', this.userToken);
    this.active.params.subscribe(params => {
      this.schId = params['id'];
    });

    if (this.schId) {
      var that = this;
      console.log('schId', this.schId);
      this.userService.getStudentData(this.schId)
        .subscribe(res => {

          if (res.status == 200 && res.body['data'] != null) {
            for (let i = 0; i < res.body['data'].length; i++) {
              let sr = {
                sr_no: i + 1
              }
              Object.assign(res.body['data'][i], sr);
            }
            res.body['data'].forEach(element => {
              this.filterData.push(element);
              this.data.push(element);

            });
            console.log('response is getting', this.data);
            setTimeout(function() {
              this.table = $("#ssTable").DataTable({
                dom: "Bfrtip",
                aoColumnDefs: [
                  {
                    bSortable: false,
                    aTargets: [-1]
                  }
                ],
                retrieve: true,
                destroy: true
              });
              that.table = this.table;
            }, 200);
          }
          else if (res.status == 202) {
            console.log('response is getting', res);
          }
        })
    }
  }


}
