import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentCheckupComponent } from './student-checkup.component';

describe('StudentCheckupComponent', () => {
  let component: StudentCheckupComponent;
  let fixture: ComponentFixture<StudentCheckupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentCheckupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentCheckupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
