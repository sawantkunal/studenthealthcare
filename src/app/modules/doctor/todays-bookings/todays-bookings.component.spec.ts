import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodaysBookingsComponent } from './todays-bookings.component';

describe('TodaysBookingsComponent', () => {
  let component: TodaysBookingsComponent;
  let fixture: ComponentFixture<TodaysBookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodaysBookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodaysBookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
