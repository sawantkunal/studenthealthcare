import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { ToastrService } from 'ngx-toastr';

declare let $;
@Component({
  selector: 'app-todays-bookings',
  templateUrl: './todays-bookings.component.html',
  styleUrls: ['./todays-bookings.component.css']
})
export class TodaysBookingsComponent implements OnInit {
  dropdownSettings: any;
  userToken: any;
  User_id: any;
  userId: any;
  schId: any;
  school_List: any;
  filterData = [];
  data = [];
  getHealthCheckupDone: any;
  getHealthCheckupPending: any;
  getStudent: any;
  table :any;

  constructor(private active: ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    this.userId = this.userToken.user_id;
    this.active.params.subscribe(params => {
      this.schId = params['id'];
      
    });
    if(this.userId){
      this.userService.getAssignedSchool(this.userId)
        .subscribe(res =>{
          if(res.status == 200){
            for (let i = 0; i < res.body['data'].length; i++) {
              let sr = {
                sr_no: i + 1
              }
              Object.assign(res.body['data'][i], sr);
            }
            this.data = res.body['data'];
            var that = this;
            setTimeout(function() {
                  this.table = $("#dasTable").DataTable({
                    dom: "Bfrtip",
                    aoColumnDefs: [
                      {
                        bSortable: false,
                        aTargets: [-1]
                      }
                    ],
                    retrieve: true,
                    destroy: true
                  });
                  that.table = this.table;
                }, 200);
              
          console.log('getAssignedSchool response is getting', res.body);
          }
          else if(res.status == 202){
            console.log(' getAssignedSchool response is getting', res);
          }
        })
    }
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'Id',
      textField: 'School_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 15,
      allowSearchFilter: true
    };
  }
  getStudentList() {
    this.userService.getSchool()
      .subscribe(res => {
        this.school_List = res.body['data'];
      })
  }

  search(term: string) {
    if (!term) {
      this.filterData = this.data;
    } else {
      this.filterData = this.data.filter(x =>
        x.School_name.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }


  getAllData() {

    this.userService.getStudents()
      .subscribe(res => {
        // if (res['status'] == "true") {
        //   this.getStudent = res['data'];
        // }
      })

    this.userService.getHealthCheckup(this.userToken.jwt_token, this.userToken.user_type_id,this.schId)
      .subscribe(res => {
        this.getHealthCheckupDone = res['completed']
        this.getHealthCheckupPending = res['pending']
      })
  }

}
