import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, } from '@angular/router';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-school-report',
  templateUrl: './school-report.component.html',
  styleUrls: ['./school-report.component.css']
})
export class SchoolReportComponent implements OnInit {


  dropdownSettings:any;
  userToken:any;
  User_id:any;
  userId:any;
  school_List:any;
  sname:any;
  data = [];
  filterData = []
  
  constructor(private active:ActivatedRoute, private userService: UserService) { }

  ngOnInit() {
    this.userToken = JSON.parse(localStorage.getItem("user"))
    // console.log('userForm controls',this.f);
    this.active.params.subscribe(params => {
      this.userId = params['id'];
      this.User_id = params['userid'];
    });

    this.getStudentHealthBySchool();
  }

  getStudentHealthBySchool(){
    this.userService.getStudentHealthByDoctor(this.userToken[0].Jwt_token,this.userToken[0].User_type,this.userToken[0].Id)
      .subscribe(res =>{
       
        res['data']['health'].forEach(element =>{
          this.data.push(element);
          this.filterData.push(element);
       
        })
      })
  }

}
