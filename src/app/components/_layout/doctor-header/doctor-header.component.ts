import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-header',
  templateUrl: './doctor-header.component.html',
  styleUrls: ['./doctor-header.component.css']
})
export class DoctorHeaderComponent implements OnInit {
  userAuth: any;
  Username:any;
  constructor() { }

  ngOnInit() {
    this.userAuth = JSON.parse(localStorage.getItem("user"))
    console.log('this.userAuth.fname', this.userAuth.fname)
    this.Username = this.userAuth.fname;
  }

}
