import { Component, OnInit } from '@angular/core';

declare let $;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  name: string;
  user: any;
  userpic: string;
  userAuth: any;
  hideShow: boolean = false;
  Username:any;
  constructor() { }

  ngOnInit() {
    this.toggleMenu();
    this.userAuth = JSON.parse(localStorage.getItem("user"))
    console.log('this.userAuth.fname', this.userAuth.fname)
    this.Username = this.userAuth.fname;
    
    if (this.userAuth && this.userAuth.data != null) {
      console.log('this.userAuth.fname', this.userAuth)

      this.hideShow = true
    }
    // if (localStorage.getItem('user')) {
    //   this.user = JSON.parse(localStorage.getItem('user'));
    //   console.log(this.user);
    //   this.name = this.user.firstName + ' ' + this.user.lastName;
    //   this.userpic = this.user.photoURL;
    // }

  }

  addRemoveClass() {
    $('.icon-circle').toggleClass("far fa");
    $('.icon-circle1').removeClass("fa");
    $('.icon-circle1').addClass("far");
  }
  addRemoveClass1() {
    $('.icon-circle1').toggleClass("far fa");
    $('.icon-circle2').removeClass("fa");
    $('.icon-circle2').addClass("far");
  }
  addRemoveClass2() {
    $('.icon-circle2').toggleClass("far fa");
    $('.icon-circle1').removeClass("fa");
    $('.icon-circle1').addClass("far");
  }
  addRemoveClass3() {
    $('.icon-circle3').toggleClass("far fa");
    $('.icon-circle4').removeClass("fa");
    $('.icon-circle4').addClass("far");

  }
  addRemoveClass4() {
    $('.icon-circle4').toggleClass("far fa");
    $('.icon-circle3').removeClass("fa");
    $('.icon-circle3').addClass("far");
  }
  addRemoveClass5() {
    $('.icon-circle5').toggleClass("far fa");
    $('.icon-circle6').removeClass("fa");
    $('.icon-circle6').addClass("far");
  }
  addRemoveClass6() {
    $('.icon-circle6').toggleClass("far fa");
    $('.icon-circle5').removeClass("fa");
    $('.icon-circle5').addClass("far");
  }
  addRemoveClass7() {
    $('.icon-circle7').toggleClass("far fa");
    $('.icon-circle6').removeClass("fa");
    $('.icon-circle6').addClass("far");
  }
  addRemoveClass8(){
    $('.icon-circle8').toggleClass("far fa");
    $('.icon-circle7').removeClass("fa");
    $('.icon-circle7').addClass("far");
  }
  addRemoveClass9(){
    $('.icon-circle9').toggleClass("far fa");
    $('.icon-circle8').removeClass("fa");
    $('.icon-circle8').addClass("far");
  }
  singOut() {

    this.userAuth = localStorage.removeItem("user");
    // location.reload();
    console.log('localStorage.removeItem("user");', localStorage.removeItem("user"))
    // this.toastr.success("Logout");
  }

  toggleMenu() {
    $(".treeview-class").click(function () {
      $(".treeview-menu-class").slideToggle();
      $(".treeview-menu-school").slideUp();
      $(".treeview-menu-doctor").slideUp();
      $(".treeview-menu-checkup").slideUp();
      $(".treeview-menu-student").slideUp();
      $(".treeview-menu-checkuplist").slideUp();
      $(".treeview-menu-checkuplist").removeClass("down");
      $(".treeview-school").removeClass("down");
      $(".treeview-doctor").removeClass("down");
      $(".treeview-checkup").removeClass("down");
      $(".treeview-student").removeClass("down");
      $(".treeview-checkuplist").removeClass("down");
      $(this).toggleClass("down");
    });
    $(".treeview-school").click(function () {
      $(".treeview-menu-school").slideToggle();
      $(".treeview-menu-class").slideUp();
      $(".treeview-menu-doctor").slideUp();
      $(".treeview-menu-checkup").slideUp();
      $(".treeview-menu-student").slideUp();
      $(".treeview-menu-checkuplist").slideUp();
      $(".treeview-menu-checkuplist").removeClass("down");
      $(".treeview-class").removeClass("down");
      $(".treeview-doctor").removeClass("down");
      $(".treeview-checkup").removeClass("down");
      $(".treeview-student").removeClass("down");
      $(".treeview-checkuplist").removeClass("down");
      $(this).toggleClass("down");
    });
    $(".treeview-doctor").click(function () {
      $(".treeview-menu-doctor").slideToggle();
      $(".treeview-menu-school").slideUp();
      $(".treeview-menu-class").slideUp();
      $(".treeview-menu-checkup").slideUp();
      $(".treeview-menu-student").slideUp();
      $(".treeview-menu-checkuplist").slideUp();
      $(".treeview-menu-checkuplist").removeClass("down");
      $(".treeview-school").removeClass("down");
      $(".treeview-class").removeClass("down");
      $(".treeview-checkup").removeClass("down");
      $(".treeview-student").removeClass("down");
      $(".treeview-checkuplist").removeClass("down");
      $(this).toggleClass("down");
    });
    $(".treeview-checkup").click(function () {
      $(".treeview-menu-checkup").slideToggle();
      $(".treeview-menu-doctor").slideUp();
      $(".treeview-menu-school").slideUp();
      $(".treeview-menu-class").slideUp();
      $(".treeview-menu-student").slideUp();
      $(".treeview-menu-checkuplist").slideUp();
      $(".treeview-menu-checkuplist").removeClass("down");
      $(".treeview-school").removeClass("down");
      $(".treeview-class").removeClass("down");
      $(".treeview-doctor").removeClass("down");
      $(".treeview-student").removeClass("down");
      $(".treeview-checkuplist").removeClass("down");
      $(this).toggleClass("down");
    });
    $(".treeview-student").click(function () {
      $(".treeview-menu-student").slideToggle();
      $(".treeview-menu-checkup").slideUp();
      $(".treeview-menu-doctor").slideUp();
      $(".treeview-menu-school").slideUp();
      $(".treeview-menu-class").slideUp();
      $(".treeview-menu-checkuplist").slideUp();
      $(".treeview-school").removeClass("down");
      $(".treeview-class").removeClass("down");
      $(".treeview-checkup").removeClass("down");
      $(".treeview-doctor").removeClass("down");
      $(".treeview-menu-checkuplist").removeClass("down");
      $(".treeview-checkuplist").removeClass("down");
      $(this).toggleClass("down");
    });
    $(".treeview-checkuplist").click(function () {
      $(".treeview-menu-checkuplist").slideToggle();
      $(".treeview-menu-checkup").slideUp();
      $(".treeview-menu-doctor").slideUp();
      $(".treeview-menu-school").slideUp();
      $(".treeview-menu-class").slideUp();
      $(".treeview-school").removeClass("down");
      $(".treeview-class").removeClass("down");
      $(".treeview-checkup").removeClass("down");
      $(".treeview-doctor").removeClass("down");
      $(this).toggleClass("down");
    });
    $(".treeview-menu-upload").click(function () {
      $(".treeview-menu-checkup").slideUp();
      $(".treeview-menu-doctor").slideUp();
      $(".treeview-menu-school").slideUp();
      $(".treeview-menu-class").slideUp();
      $(".treeview-menu-student").slideUp();
      $(".treeview-menu-checkuplist").slideUp();
      $(".treeview-school").removeClass("down");
      $(".treeview-class").removeClass("down");
      $(".treeview-checkup").removeClass("down");
      $(".treeview-doctor").removeClass("down");
      $(".treeview-checkuplist").removeClass("down");
      $(".treeview-student").removeClass("down");
      $(this).toggleClass("down");
    });
  }

}
