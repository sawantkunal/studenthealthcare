import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-school-header',
  templateUrl: './school-header.component.html',
  styleUrls: ['./school-header.component.css']
})
export class SchoolHeaderComponent implements OnInit {

  userAuth: any;
  Username:any;
  constructor() { }

  ngOnInit() {
    this.userAuth = JSON.parse(localStorage.getItem("user"))
    console.log('this.userAuth.fname', this.userAuth.fname)
    this.Username = this.userAuth.fname;
  }

}
