import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  userForm: FormGroup;
  formData:any;
  submitted = false;
  userToken:any;
  userId:any;

  constructor(private toastr: ToastrService,private router:Router,private active: ActivatedRoute, private userService:UserService,private _fb: FormBuilder,){ }

  ngOnInit(){
    this.userFormData();

    // this.userToken = JSON.parse(localStorage.getItem("user"))
    // if(this.userToken){
    //   this.userId = this.userToken.user_id;
    //   //this.router.navigate(['home']);
    // }
  }

  userLogin(){
    // var formData = JSON.stringify(this.g.value)
    
    this.submitted = true;
    if (this.g.value.email && this.g.value.password){
      this.submitted = false;
      // console.log('this.val', this.g.value)
      this.userService.Login(this.g.value)
        .subscribe(res =>{
          console.log('response login', res)

          if(res.status == 200){
            localStorage.setItem("user",JSON.stringify(res.body['data']));
            this.toastr.success(res.body['message'],'',{timeOut:600});
            if(res.body['data'].user_type_id == '1'){
              this.router.navigate(['/admin-dashboard']);
            }
            if(res.body['data'].user_type_id == '2'){
              this.router.navigate(['/doctor-dashboard']);
            }
            if(res.body['data'].user_type_id == '3'){
              this.router.navigate(['/school_admin']);
            }
          }else if(res.status == 202){
            this.toastr.warning(res.body['message'],'',{timeOut:600});
          }
        })
    }

  }

  userFormData(){
    this.userForm = this._fb.group({
      email:['', Validators.compose([Validators.required, Validators.email])],
      password:['',Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
    })
  }
  get f() { return this.userForm.controls; }
  get g() { return this.userForm}
}