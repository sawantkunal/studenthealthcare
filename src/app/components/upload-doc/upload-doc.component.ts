import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-upload-doc',
  templateUrl: './upload-doc.component.html',
  styleUrls: ['./upload-doc.component.css']
})
export class UploadDocComponent implements OnInit {

  constructor(private toastr: ToastrService,private router:Router,private active: ActivatedRoute, private userService:UserService,private _fb: FormBuilder) { }
  uploadForm: FormGroup;
  formData:any;
  submitted = false;
  userToken:any;
  userId:any;
  fileName: any;
  school_list:any;
  classList :any;
  isstudent = false;
  disableButton = false;
  loader= false;
  ngOnInit() {
    this.uploadFormData();
  }
  uploadFileData() {
    
    this.submitted = true;
    console.log('this.uploadForm.value', this.uploadForm.value)
    if (this.uploadForm.valid) {
      this.submitted = false;
      this.disableButton = true;
      this.loader = true;
        var formData: any = new FormData()
        formData.append("file", this.fileName);
        formData.append('data', JSON.stringify(this.uploadForm.value))
        this.userService.uploadcsv(formData)
          .subscribe(res => {
            console.log('response',res)
            if (res.status == 200) {
              this.toastr.success(res.body['message'],'',{timeOut:600});
              this.disableButton = false;
              this.loader = false;
              //this.router.navigate(['school_list']);
            }else{
              this.toastr.error(res.body['message'],'',{timeOut:600});
              this.disableButton = false;
              this.loader = false;
            }
            this.loader = false;
            console.log('response file import', res.body)
          })
      }
    }

    getSchoolList(event) {
      if(event.target.value=='student'){
        this.isstudent = true;
        this.userService.getSchool()
          .subscribe(res => {
            if (res.status == 200) {
              this.school_list = res.body['data'];
            }
            console.log('getSchoolList', res);
          })
      }else{
        this.isstudent = false;
      }
    }

    getClass(event) {
      this.classList = null;
      this.uploadForm.value.class_id = null;
      this.userService.getClass(event.target.value)
        .subscribe(res => {
          if(res.body && res.body['data'].length == 0){
            this.toastr.warning("There is no more classes for this school");
            this.classList = res.body['data'];
          }else if(res.body && res.body['data'].length > 0){
            this.classList = res.body['data'];
            console.log('getClass', res.body['data'])
          }
        })
    }

  handleChangeImage(event) {
    this.fileName = event.target.files[0];
    //this.uploadForm.value.fileupload = event.target.files[0];
    console.log('event handle', this.fileName);

  }
  uploadFormData(){
    this.uploadForm = this._fb.group({
      user:['', Validators.compose([Validators.required])],
      fileupload:['',Validators.compose([Validators.required])],
      // class_id:[''],
      // school_id:['']
    })
  }
  get f() { return this.uploadForm.controls; }
  get g() { return this.uploadForm.value; }
}
