import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recoverpassword-email',
  templateUrl: './recoverpassword-email.component.html',
  styleUrls: ['./recoverpassword-email.component.css']
})
export class RecoverpasswordEmailComponent implements OnInit {

  submitted = false;
  EmailForm : FormGroup;
  loader= false;
  constructor(private toastr: ToastrService, private router: Router, private userService: UserService, private _fb: FormBuilder, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.EmailFormData();
  }

  sendemail(){
    this.submitted = true;
    console.log('this.EmailForm',this.EmailForm);
    if(this.EmailForm.valid){
      this.loader = true;
      console.log('status',this.EmailForm.valid);
      this.submitted = false;
      this.userService.sendpasswordlink(this.g.value.email)
        .subscribe(res =>{
          console.log('response login', res)

          if(res.status == 200){
            this.toastr.success(res.body['message'],'',{timeOut:600});
            this.router.navigate(['/']);
          }else if(res.status == 202){
            this.toastr.warning(res.body['message'],'',{timeOut:600});
            this.loader = false;
          }
        })
    }else{
      console.log('status',this.EmailForm.valid);
    }
  }
  EmailFormData() {
    this.EmailForm = this._fb.group({
      email:['', Validators.compose([Validators.required, Validators.email])],
    })
  }
  
  get f() { return this.EmailForm.controls; }
  get g() { return this.EmailForm}
}
