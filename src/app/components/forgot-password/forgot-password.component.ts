import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  PasswordFormData: FormGroup;
  token;
  submitted = false;
  matchPassword = false;

  constructor(
    private toastr: ToastrService, 
    private router: Router, 
    private userService: UserService, 
    private _fb: FormBuilder, 
    private activatedRoute: ActivatedRoute) { 
    // this.activatedRoute.queryParams.subscribe(params => {
    //   let token = params['id'];
    //   console.log(token); // Print the parameter to the console. 
    // });

  }

  ngOnInit() {
    this.passwordFormData();
    this.activatedRoute.params.subscribe(params => {
       this.token = params['id'];
      console.log("ID",params['id']);
    });
  }


  changePassSubmit() {
    this.submitted = true;
    if (this.PasswordFormData.valid) {
      if (this.PasswordFormData.value.password != this.PasswordFormData.value.conf_password) {
        this.matchPassword = true;
      } else if (this.PasswordFormData.value.password == this.PasswordFormData.value.conf_password) {
        this.submitted = false;
        this.matchPassword = false;

        var data = {
          token: this.token,
          password: this.PasswordFormData.value.password,
        }
          this.userService.ForgotPass(data)
            .subscribe(res =>{
              if(res.status == 200){
                localStorage.setItem("user",JSON.stringify(res.body['data']));
                this.toastr.success(res.body['message'],'',{timeOut:600});
                
                this.router.navigate(['/login']);
    
              }else if(res.status == 202){
                this.toastr.warning(res.body['message'],'',{timeOut:600});
              }
            })

      }
    }
  }


  passwordFormData() {
    this.PasswordFormData = this._fb.group({
      password: ['', Validators.compose([Validators.required])],
      conf_password: ['', Validators.compose([Validators.required,])],
    })
  }

  get f() { return this.PasswordFormData.controls; }
  get g() { return this.PasswordFormData.value; }
}
