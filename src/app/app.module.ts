import { httpInterceptorProviders } from './http-interceptors/index';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgSelectComponent } from '@ng-select/ng-select';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ProgressBarModule} from "angular-progress-bar";
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AdminLayoutComponent } from './components/_layout/admin-layout/admin-layout.component';
import { SchoolLayoutComponent } from './components/_layout/school-layout/school-layout.component';
import { StudentLayoutComponent } from './components/_layout/student-layout/student-layout.component';
import { DoctorsLayoutComponent } from './components/_layout/doctors-layout/doctors-layout.component';
import { StudentHeaderComponent } from './components/_layout/student-header/student-header.component';
import { DoctorHeaderComponent } from './components/_layout/doctor-header/doctor-header.component';
import { SchoolHeaderComponent } from './components/_layout/school-header/school-header.component';
import { HeaderComponent } from './components/_layout/admin-header/header.component';


import { DashboardComponent } from './modules/super-admin/dashboard/dashboard.component';

import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';

// import { DoctorModule } from './modules/doctor/doctor.module';
// import { SchoolAdminModule } from './modules/school-admin/school-admin.module';
// import { SuperAdminModule } from './modules/super-admin/super-admin.module';
import { ChartsModule } from 'ng2-charts';
import { DoctorDashboardComponent } from './modules/doctor/doctor-dashboard/doctor-dashboard.component';
import { SchoolDashboardComponent } from './modules/school-admin/school-dashboard/school-dashboard.component';
import { ReportHistoryComponent } from './components/report-history/report-history.component';
import { AuthAdminGuard } from './guards/auth-admin.guard';
import { AuthDoctorGuard } from './guards/auth-doctor.guard';
import { AuthSchoolGuard } from './guards/auth-school.guard';

import { UserService } from './services/user.service';
// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DoctorAssignedSchoolsComponent } from './modules/doctor/doctor-assigned-schools/doctor-assigned-schools.component';
import { SchoolAdminComponent } from './modules/school-admin/school-admin/school-admin.component'

import { AddSchoolComponent } from './modules/super-admin/add-school/add-school.component';
import {UpdateSchoolComponent} from './modules/super-admin/update-school/update-school.component';
import { SchoolListComponent } from './modules/super-admin/school-list/school-list.component';
import { AddStudentComponent } from './modules/super-admin/add-student/add-student.component';
import { StudentListComponent } from './modules/super-admin/student-list/student-list.component';
import { AddDoctorComponent } from './modules/super-admin/add-doctor/add-doctor.component';
import { DoctorListComponent } from './modules/super-admin/doctor-list/doctor-list.component';
import { StudentProfileComponent } from './modules/super-admin/student-profile/student-profile.component';
import { DoctorProfileComponent } from './modules/super-admin/doctor-profile/doctor-profile.component';
import { SchoolDetailsComponent } from './modules/super-admin/school-details/school-details.component'
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {DataTableModule} from "angular-6-datatable";
import { StudentReportComponent } from './modules/doctor/student-report/student-report.component';
import { StudentCheckupComponent } from './modules/doctor/student-checkup/student-checkup.component';
import { SchoolReportComponent } from './components/school-report/school-report.component';
import { UploadDocComponent } from './components/upload-doc/upload-doc.component';
import { AddClassesComponent } from './modules/super-admin/add-classes/add-classes.component';
import { ClassListComponent } from './modules/super-admin/class-list/class-list.component';
import { ScheduleCheckupComponent} from './modules/super-admin/schedule-checkup/schedule-checkup.component';
import { ScheduleListComponent} from './modules/super-admin/schedule-list/schedule-list.component';

import { StudentListComponents } from './modules/school-admin/student-list/student-list.component';
import { StudentProfileComponents } from './modules/school-admin/student-profile/student-profile.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { SchoolstudentListComponent } from './modules/doctor/schoolstudent-list/schoolstudent-list.component';
import { PastassignedschoolListComponent } from './modules/doctor/pastassignedschool-list/pastassignedschool-list.component';
import { PastschoolstudentListComponent } from './modules/doctor/pastschoolstudent-list/pastschoolstudent-list.component';
import { PaststudentcheckupComponent } from './modules/doctor/paststudentcheckup/paststudentcheckup.component';
import { RecoverpasswordEmailComponent } from './components/recoverpassword-email/recoverpassword-email.component';
import { CheckupDoneListComponent } from './modules/super-admin/checkup-done-list/checkup-done-list.component';
import { StudentDonecheckupListComponent } from './modules/super-admin/student-donecheckup-list/student-donecheckup-list.component';
import { StudentCheckupDetailComponent } from './modules/super-admin/student-checkup-detail/student-checkup-detail.component';
import { AddDivisionComponent } from "./modules/super-admin/add-division/add-division.component";
import { TodaysBookingsComponent } from "./modules/doctor/todays-bookings/todays-bookings.component";

import { Select2Module } from 'ng2-select2';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    DoctorAssignedSchoolsComponent,
    SchoolAdminComponent,
    LoginComponent,
    DoctorHeaderComponent,
    DoctorDashboardComponent,
    ScheduleCheckupComponent,
    ScheduleListComponent,
    SchoolHeaderComponent,
    SchoolDashboardComponent,
    ReportHistoryComponent,
    AddSchoolComponent,
    UpdateSchoolComponent,
    SchoolListComponent,
    ClassListComponent,
    AddStudentComponent,
    StudentListComponent,
    StudentReportComponent,
    StudentCheckupComponent,
    AddDoctorComponent,
    DoctorListComponent,
    StudentProfileComponent,
    DoctorProfileComponent,
    SchoolDetailsComponent,
    SchoolReportComponent,
    UploadDocComponent,
    AdminLayoutComponent,
    SchoolLayoutComponent,
    StudentLayoutComponent,
    DoctorsLayoutComponent,
    StudentHeaderComponent,
    AddClassesComponent,
    StudentProfileComponents,
    StudentListComponents,
    ForgotPasswordComponent,
    SchoolstudentListComponent,
    PastassignedschoolListComponent,
    PastschoolstudentListComponent,
    PaststudentcheckupComponent,
    RecoverpasswordEmailComponent,
    CheckupDoneListComponent,
    StudentDonecheckupListComponent,
    StudentCheckupDetailComponent,
    AddDivisionComponent,
    TodaysBookingsComponent
  ],
  imports: [
    BrowserModule,
    DlDateTimeDateModule,  // <--- Determines the data type of the model
    DlDateTimePickerModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    ProgressBarModule,
    // DoctorModule,
    // SchoolAdminModule,
    // SuperAdminModule,
    ChartsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    // NgMultiSelectDropDownModule,
    DataTableModule,
    NgMultiSelectDropDownModule.forRoot(),
    Select2Module


  ],

  // exports: [FormsModule, ReactiveFormsModule ],
  providers: [
      // {provide: LocationStrategy, useClass: HashLocationStrategy},
      AuthAdminGuard,
      AuthSchoolGuard,
      AuthDoctorGuard,
      UserService,
      httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
