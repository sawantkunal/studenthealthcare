import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { observable } from 'rxjs';

// var userToken = JSON.parse(localStorage.getItem("user"))
// var Token = '';
// if (userToken && userToken['jwt-token'] != null) {
//   Token = userToken['jwt-token'];

// }

const headers_object = new HttpHeaders({
  'Content-Type': 'application/json',
  // 'Authorization': Token
});

const httpOptions = {
  headers: headers_object,
};


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  Login(userData) {
    return this.http.post('login/secure', userData, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  ForgotPass(userData) {
    return this.http.post('resetpassword', userData, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  sendMail(token, usertype, email, address) {
    var data = {
      Token: token,
      usertype: usertype,
      email: email,
      address: address
    }
    return this.http.post('sendmail', data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  // School module services
  addSchool(schoolData) {
    return this.http.post('school/add', schoolData, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getSchoolList() {
    return this.http.get("school", { observe: 'response' })
      .map(res => {
        // console.log('response', res);
        return res;
      })
  }

  getSchoolSingle(User_id) {
    return this.http.get('school/showschoolbyid/' + User_id, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getSchoolId(user_id) {
    return this.http.get('school/getSchoolId/' + user_id, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  updateSchool(data) {
    return this.http.post('school/updateschoolbyid', data, { observe: 'response' })
      .map(res => {
        // console.log('update successs', res);
        return res;
      })
  }

  getDoctor() {
    return this.http.get('doctor/doctorlist', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getSchool() {
    return this.http.get('school/getschoollist', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getAllClassDivision() {
    //'school/getAllClassDivision'
    return this.http.get( 'school/getAllClassDivision' , { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  

  getClass(id) {
    return this.http.get('school/getclasslistbyschool/' + id, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getClassBySchool(schId){
    return this.http.get('school/getClassBySchoolDemo/' + schId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getDivision(id){
    return this.http.get('school/getdivisionlistbyschoolclass/' + id, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  
  getAllclass(){
    return this.http.get('school/getAllclass', { observe: 'response' })
      .map(res => {
        return res;
      })
  }  
  getAllDivision(){
    return this.http.get('school/getAlldivision', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getSchoolDivision(id,schid){
    return this.http.get('school/getdivisionlistbyclass/' + id + '/' + schid, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  // addClass(data) {
  //   return this.http.post('school/addclass', data, { observe: 'response' })
  //     .map(res => {
  //       return res;
  //     })
  // }
  addUpdateClassDivision(data) {
    return this.http.post('school/addUpdateClassDivision', data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  

  addClassDivision(data) {
    return this.http.post('school/addclassdivision', data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getClassList() {
    return this.http.get('school/getclasslist', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  // getClassSingle(id) {
  //   return this.http.get('school/getclassbyid/' + id, { observe: 'response' })
  //     .map(res => {
  //       return res;
  //     })
  // }

  getClassDivisionBySchool(id) {
    return this.http.get('school/getClassDivisionBySchool/' + id, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  updateClass(data) {
    return this.http.post('school/updateclassbyid', data, { observe: 'response' })
      .map(res => {
        return res;
      })

  }

  // updateUserStatus(token,userId){
  //   var data = {
  //     token: token,
  //     userId: userId
  //   } 
  //   return this.http.post('common/updateUserStatus', data, { observe: 'response' })
  //     .map(res => {
  //       return res;
  //     })
  // }
  updateUserStatus(userId){
    
    return this.http.get('common/updateUserStatus/' + userId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  // Schedules

  addSchoolCheckup(Data) {
    return this.http.post('/school/addschoolcheckup', Data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getScheduleDetails() {
    return this.http.get('school/getschoolcheckuplist', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getScheduleById(id) {
    return this.http.get('school/getschoolcheckupbyid/' + id, { observe: 'response' })
      .map(res => {
        return res;
      })

  }

  updateScheduleCheckup(Data) {
    return this.http.post('/school/updateschoolcheckup', Data, { observe: 'response' })
      .map(res => {
        return res;
      })

  }

  // Doctor module services
  addDoctor(doctorData) {
    return this.http.post('doctor/add', doctorData, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getDoctorList() {
    return this.http.get('doctor', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getDoctorSingle(id) {
    return this.http.get('doctor/showdoctorbyid/' + id, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  updateDoctor(data) {
    return this.http.post('doctor/updatesdoctorbyid', data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  addStudent(studentData) {
    return this.http.post('student/add', studentData, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getStudentList() {
    // var data ={
    //   Token:token,
    //   usertype:id
    // }
    return this.http.get('student', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getStudentSingle(id) {
    // console.log('data', data);
    return this.http.get('student/getstudentbyid/' + id, { observe: 'response' })
      .map(res => {
        console.log('data is hwre', res);
        return res;
      })
  }

  getStudentData(id) {
   
    return this.http.get('doctor/getstudentbyschool/' + id, { observe: 'response' })
      .map(res => {
        console.log('data is hwre', res);
        return res;
      })
  }
  getPastStudentData(id) {
    var data = {
      id: id
    }
    return this.http.post('school/getpastschoolstudents', data, { observe: 'response' })
      .map(res => {
        console.log('data is here', res);
        return res;
      })
  }
  getCheckupDetail(id) {
    var data = {
      id: id
    }
    return this.http.post('school/getcheckupdetailsbyId', data, { observe: 'response' })
      .map(res => {
        console.log('data is here', res);
        return res;
      })
  }
  showStudentsHealth(token, usertype, User_id) {
    var data = {
      Token: token,
      usertype: usertype,
      userid: User_id
    }

    return this.http.post('student/gethealthdata', data, { observe: 'response' })
      .map(res => {
        // console.log('data is hwre', res);
        return res;
      })

  }

  updateStudent(data) {
    return this.http.post('student/updatesstudentbyid', data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getSchools() {
    return this.http.get('dashboard/getschools', { observe: 'response' })
      // return this.http.get('dashboard/getschools/')
      .map(res => {
        // console.log('dashboard/getschools',res)
        return res;
      })
  }
  getDoctors() {
    return this.http.get('dashboard/getdoctors', { observe: 'response' })
      // return this.http.get('dashboard/getdoctors?Token='+ Token, {observe: 'response' as 'body'})
      .map(res => {
        // console.log('dashboard/getdoctors',res)
        return res;
      })
  }
  getStudents() {
    return this.http.get('dashboard/getstudents', { observe: 'response' })
      // return this.http.get('dashboard/getstudents?Token='+ Token, {observe: 'response' as 'body'})
      .map(res => {
        // console.log('dashboard/getstudents',res)
        return res;
      })
  }

  getCheckups() {
    return this.http.get('dashboard/getcheckups', { observe: 'response' })
      // return this.http.get('dashboard/getcheckups?Token='+ Token, {observe: 'response' as 'body'})
      .map(res => {
        console.log('dashboard/getcheckups', res)
        return res;

      })
  }

  getHealthCheckup(token, usertype, school_id) {
    var data = {
      Token: token,
      usertype: usertype,
      school_id: school_id,
    }
    return this.http.post('student/gethealthcheckup', data)
      .map(res => {
        return res;
      })
  }

  //Doctor Dashboard

  getDoctorSchool(token, usertype, id) {
    var data = {
      token: token,
      usertype: usertype,
      id: id
    }
    return this.http.post('doctor/getdoctorschool', data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  // getPastDoctorSchool(token, usertype, id) {
  //   var data = {
  //     token: token,
  //     usertype: usertype,
  //     id: id
  //   }
  //   return this.http.post('doctor/getpastdoctorschool', data, { observe: 'response' })
  //     .map(res => {
  //       return res;
  //     })
  // }

  getStudentBySchool(token, usertype, sname) {
    var data = {
      Token: token,
      usertype: usertype,
      sname: sname
    }
    return this.http.post('doctor/getstudentbyschool', data)
      .map(res => {
        return res;
      })

  }

  getStudentBySchoolId(token, usertype, school_id) {
    var data = {
      token: token,
      usertype: usertype,
      school_id: school_id
    }
    return this.http.post('school/getstudentbyschool', data, { observe: 'response' })
      .map(res => {
        return res;
      })

  }
  getBloodGroup() {
    return this.http.get('student/getBloodGroup', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  // getBloodGroupbyId(data){
  //   return this.http.post('student/getBloodGroupbyId',data, { observe: 'response' })
  //     .map(res => {
  //       return res;
  //     })
  // }

  postStudentCheckup(data) {
    return this.http.post('student/addstudentdisease', data)
      .map(res => {
        return res;
      })
  }

  getParentEmails(data) {
    return this.http.get('student/getparentemails/' + data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getParentDetails(data) {
    return this.http.get('student/getparentdetails/' + data, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getStudentHealthBySchool(token, usertype, userid) {
    var data = {
      Token: token,
      usertype: usertype,
      id: userid
    }
    // console.log('getStudentHealthBySchool', data);
    return this.http.post('student/getstudenthealthbyschool', data)
      .map(res => {
        return res;
      })
  }

  getStudentHealthByDoctor(token, usertype, userid) {
    var data = {
      Token: token,
      usertype: usertype,
      id: userid
    }
    // console.log('getStudentHealthBySchool', data);
    return this.http.post('school/getstudenthealthbyschool', data)
      .map(res => {
        return res;
      })
  }
  sendpasswordlink(email){
    var data = {
      email: email,
    }

    return this.http.post('passwordrecoveryemail', data, { observe: 'response' })
      .map(res => {
        return res;
    })
  }
  getDonecheckuplist(){
    return this.http.get('school/getdoneschoolcheckuplist', { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getcheckupStudentList(schId){
    return this.http.get('school/getcheckupStudentList/'+schId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getcheckupStudentListByID(schId){
    return this.http.get('school/getcheckupStudentListByID/'+schId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  

  uploadcsv(fileData){
    return this.http.post('common/importcsv', fileData, { observe: 'response' })
      .map(res => {
        return res;
    })
  }
  getSchoolStudentCount(schId){
    return this.http.get('school/getschoolstudents/'+schId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getScheduledCheckup(checkupId){
    return this.http.get('school/getScheduledCheckup/'+checkupId, { observe: 'response' })
      .map(res => {
        return res; 
      })
  }
  getUpcomingSchool(userId){
    return this.http.get('doctor/getUpcomingSchool/'+userId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }

  getAssignedSchool(userId){
    return this.http.get('doctor/getAssignedSchool/'+userId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getClassDivision(classId){
    return this.http.get('school/getClassDivision/'+classId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getpastdoctorschool(userId){
    return this.http.get('doctor/getPastDoctorSchools/'+userId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getDoctorData(userId){
    return this.http.get('doctor/getDoctorDashoboadData/'+userId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  getSchoolData(userId){
    return this.http.get('school/getSchoolDashboardData/'+userId, { observe: 'response' })
      .map(res => {
        return res;
      })
  }
  
}
