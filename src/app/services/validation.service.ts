import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';


var headers = new HttpHeaders();
headers.append('Content-Type', 'application/json');
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Origin', '*');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type");

const httpOptions = {
  headers: new HttpHeaders({
    'enctype': 'multipart/form-data',
    'Accept': 'application/json'
  })
};


@Injectable({
  providedIn: 'root'
})


export class ValidationService {

  constructor(private http: HttpClient) { }

// http://connexistech.net/shc-background/public/dashboard/checkemail
// http://connexistech.net/shc-background/public/dashboard/checkmobile

  checkEmail(email){
    return this.http.post('common/checkemail', email, {observe:'response'})
    .map(res => {
      console.log('response email', res);
      return res;
    })
  }

  checkMobile(mobile){
    let data ={
      mobile:mobile
    }
    return this.http.post('common/checkmobile', data, {observe:'response'})
    .map(res => {
      console.log('response', res);
      return res;
    })
  }

  checkSchool(school){
  
    return this.http.post('common/checkifschoolexist', school, {observe:'response'})
    .map(res => {
      console.log('response', res);
      return res;
    })
    
  }

}
