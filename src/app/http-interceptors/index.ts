import { APIInterceptor } from './APIInterceptor.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
export const httpInterceptorProviders = [
    {provide:HTTP_INTERCEPTORS,useClass:APIInterceptor,multi:true}
]